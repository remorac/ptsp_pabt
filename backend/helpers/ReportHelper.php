<?php

namespace backend\helpers;

use yii\helpers\Url;
use backend\models\ReportSigner;

class ReportHelper
{
	public static function months() {
		return [
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember',
		];
	}
	
	public static function header($title, $to_pdf = false) {
		$imageWidth 	= $to_pdf ? '70px' : '50%';
		$marginBottom 	= $to_pdf ? '10px' : '20px';
		return '
			<table width="100%" style="margin-bottom:10px">
				<tr>
					<td align="left" valign="center" width="15%"><img src="'.\Yii::getAlias('@web/img/logo.png').'" width="'.$imageWidth.'"></td>
					<td align="center" valign="center">
						<font size="4">BADAN NARKOTIKA NASIONAL PROVINSI SUMATERA BARAT</font>				
						<br><font size="5">INVENTARIS BARANG ELEKTRONIK DAN MESIN</font>
						<br>
						<br>'.$title.'
					</td>
					<td width="15%">&nbsp;</td>
				</tr>
			</table>
		';
	}

	public static function footer($to_pdf = false) {
		$fontSize 	= $to_pdf ? 'font-size:11px' : '';

		$operator 	= ReportSigner::findOne(['jabatan' => 'Operator']);
		$kasubag 	= ReportSigner::findOne(['jabatan' => 'Kasubag Sarana dan Prasarana']);
		$kabag 		= ReportSigner::findOne(['jabatan' => 'Kabag Umum']);

		return '
			<table width="100%" style="margin-bottom:20px;'.$fontSize.'">
				<tr><td colspan=3 class="text-center">Padang, '.date('d F Y').'</td></tr>
				<tr>
					<td width="33%" align="center" >						
						<br>Dibuat oleh
						<br><b>'.$operator->jabatan.'</b>
						<br>
						<br>
						<br>
						<br>
						<br><b><u>'.$operator->nama_pejabat.'</u></b>
						<br>'.$operator->nip.'
					</td>
					<td width="34%" align="center">						
						<br>Diperiksa oleh
						<br><b>'.$kasubag->jabatan.'</b>
						<br>
						<br>
						<br>
						<br>
						<br><b><u>'.$kasubag->nama_pejabat.'</u></b>
						<br>'.$kasubag->nip.'
					</td>
					<td width="33%" align="center">
						<br>Diketahui oleh
						<br><b>'.$kabag->jabatan.'</b>
						<br>
						<br>
						<br>
						<br>
						<br><b><u>'.$kabag->nama_pejabat.'</u></b>
						<br>'.$kabag->nip.'
					</td>
				</tr>
			</table>
		';
	}
}
