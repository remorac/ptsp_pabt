<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TrainingEvent */

$this->title = 'Update Training Event: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Training Event', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="training-event-update box-- box-warning--">

    <!-- <div class="box-header"></div> -->

    <?= $this->render('_form-update', [
        'model' => $model,
        'modelItem' => $modelItem,
    ]) ?>

</div>
