<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TrainingEvent */

$this->title = 'Create Training Event';
$this->params['breadcrumbs'][] = ['label' => 'Training Event', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-event-create box-- box-success--">
	<!-- <div class="box-header"></div> -->

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>
    
</div>
