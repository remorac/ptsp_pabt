<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\SwitchInput;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use backend\models\TrainingEvent;
use backend\models\TrainingField;
use backend\models\TrainingParticipant;
use backend\models\TrainingEventParticipant;

/* @var $this yii\web\View */
/* @var $model backend\models\Incoming */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="incoming-form">

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne" style="border-bottom: none">
            <h4 class="panel-title">
                <?= '<b>' . $model->name . '</b> : ' . $model->trainingField->name .', <small>'. $model->start_date .' &raquo; '. $model->location.'</small>' ?>
                <a class="pull-right btn btn-default btn-text-warning btn-xs" style="margin-top:-2px" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    <small><i class="glyphicon glyphicon-pencil text-warning"></i></small>
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body" style="background: #fdfdfd">
                
                <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'training_field_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(TrainingField::find()->all(), 'id', 'name'),
                    'options' => ['placeholder' => ''],
                    'pluginOptions' => ['allowClear' => true],
                ]); ?>

                <?= $form->field($model, 'start_date')->widget(DatePicker::classname(), [
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'readonly' => true,
                    'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
                ]); ?>

                <?= $form->field($model, 'end_date')->widget(DatePicker::classname(), [
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                    'readonly' => true,
                    'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
                ]); ?>

                <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>
                
                <div class="form-panel">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                            <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
    
    <div class="panel-- panel-default-- panel-body--">
        <div class="row">
            <div class="col-sm-12">

                <?php $form = ActiveForm::begin(); ?>

                <table width="100%" class="table-form">
                    <tr>
                        <td>
                            <?= $form->field($modelItem, 'training_participant_id')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(TrainingParticipant::find()->all(), 'id', 'name'),
                                'options' => ['placeholder' => ''],
                                'pluginOptions' => ['allowClear' => true],
                            ]); ?>
                        </td>
                        <td>
                            <?= $form->field($modelItem, 'is_approved')->textInput() ?>
                        </td>
                        <td>
                            <?= $form->field($modelItem, 'remark')->textInput() ?>
                        </td>
                        <td width="184px">
                            <label class="control-label">&nbsp;</label>
                            <?php 
                                if ($modelItem->isNewRecord) {
                                    echo Html::submitButton('<i class="glyphicon glyphicon-plus"></i> '.Yii::t('app', 'Add'), ['class' => 'btn btn-success btn-block']);      
                                } else {
                                    echo '<div class="input-group">';
                                    echo Html::submitButton('<i class="glyphicon glyphicon-ok"></i> '.Yii::t('app', 'Update'), ['class' => 'btn btn-primary'])
                                    .' '. Html::a('<i class="glyphicon glyphicon-remove"></i> '. Yii::t('app', 'Cancel'), ['update', 'id' => $model->id], [
                                        'class' => 'btn btn-default delete-item',
                                    ]);
                                    echo "<div>";
                                }
                            ?>
                        </td>
                    </tr>
                </table>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?php             
                    $dataProvider = new \yii\data\ActiveDataProvider([
                        'query' => TrainingEventParticipant::find()->where(['training_event_id' => $model->id]),
                        'pagination' => false,
                        'sort' => [
                            'defaultOrder' => [
                                'id' => SORT_DESC,
                            ]
                        ],
                    ]);
                
                    echo \kartik\grid\GridView::widget([
                        'dataProvider' => $dataProvider,
                        // 'pjax' => true,
                        'hover' => true,
                        'striped' => false,
                        'bordered' => false,
                        'summary' => false,
                        'toolbar'=> [
                            Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']),
                            Html::a('<i class="fa fa-repeat"></i> ' . Yii::t('app', 'Reload'), ['index'], ['data-pjax'=>0, 'class'=>'btn btn-default']),
                            '{toggleData}',
                            // $exportMenu,
                        ],
                        'panel' => false,
                        'pjaxSettings' => ['options' => ['id' => 'grid']],
                        // 'filterModel' => $searchModel,
                        'rowOptions' => function($model) use ($modelItem) {
                            return $model->id == $modelItem->id ? ['style' => 'background:#ffff77'] : '';
                        },
                        'columns' => [
                            [
                                'class' => 'yii\grid\SerialColumn',
                                'headerOptions' => ['class' => 'text-right'],
                                'contentOptions' => ['class' => 'text-right'],
                            ],
                            // 'id',
                            [
                                'attribute' => 'training_participant_id',
                                'value' => 'trainingParticipant.name',
                                'filterType' => GridView::FILTER_SELECT2,
                                'filter' => ArrayHelper::map(TrainingParticipant::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                                'filterInputOptions' => ['placeholder' => ''],
                                'filterWidgetOptions' => [
                                    'pluginOptions' => ['allowClear' => true],
                                ],
                            ],
                            'is_approved:integer',
                            [
                                'attribute' => 'remark',
                                'format' => 'ntext',
                                'contentOptions' => ['class' => 'text-wrap'],
                            ],
                            [
                                'contentOptions' => ['class' => 'action-column nowrap text-right'],
                                'attribute' => '',
                                'format' => 'raw',
                                'value' => function ($data) {
                                    return 
                                    /* Html::a('<i class="glyphicon glyphicon-pencil"></i>', ['update', 'id' => $data->trainig, 'incoming_item_id' => $data->id], [
                                        'class' => 'btn btn-xs btn-default btn-text-warning',
                                    ])
                                    .' '. */
                                    Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete-item', 'id' => $data->id], [
                                        'class' => 'btn btn-xs btn-default btn-text-danger',
                                        'data-confirm' => 'Are you sure you want to delete this item?',
                                        'data-method' => 'post',
                                    ]);
                                }
                            ], 
                        ],
                    ]);
                ?>
            </div>
        </div>
    </div>

    <div class="form-panel">
        <div class="row">
            <div class="col-sm-12">
                <?= Html::a('<i class="glyphicon glyphicon-stop"></i> '. Yii::t('app', 'Done'), ['view', 'id' => $model->id], [
                    'class' => 'btn btn-default btn-text-danger',
                ]) ?>
            </div>
        </div>
    </div>

</div>

<?php 
    $this->registerJsFile(
        '@web/js/incoming-purchase.js',
        ['depends' => [\yii\web\JqueryAsset::className()]]
    );
?>