<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\SwitchInput;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use backend\models\TrainingEvent;
use backend\models\TrainingField;
use backend\models\TrainingParticipant;
use backend\models\TrainingEventParticipant;

/* @var $this yii\web\View */
/* @var $model backend\models\TrainingEvent */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Training Event', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="training-event-view box-- box-info--">

    <div class="box-body--">
        <p>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> '. 'Update', ['update', 'id' => $model->id], [
            'class' => 'btn btn-warning',
        ]) ?>
        <?= Html::a('<i class="glyphicon glyphicon-trash"></i> ' . 'Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        </p>

        <div class="detail-view-container">
        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                // 'id',
                'name',
                'trainingField.name:text:Training Field',
                'start_date',
                'end_date',
                'location',
                // 'created_at:datetime',
                // 'updated_at:datetime',
                // 'createdBy.username:text:Created By',
                // 'updatedBy.username:text:Updated By',
            ],
        ]) ?>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <?php 
                $dataProvider = new \yii\data\ActiveDataProvider([
                    'query' => TrainingEventParticipant::find()->where(['training_event_id' => $model->id]),
                    'pagination' => false,
                    'sort' => [
                        'defaultOrder' => [
                            'id' => SORT_DESC,
                        ]
                    ],
                ]);

                echo \kartik\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                        // 'pjax' => true,
                    'hover' => true,
                    'striped' => false,
                    'bordered' => false,
                    'summary' => false,
                    'toolbar' => [
                        Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']),
                        Html::a('<i class="fa fa-repeat"></i> ' . Yii::t('app', 'Reload'), ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default']),
                        '{toggleData}',
                            // $exportMenu,
                    ],
                    'panel' => false,
                    'pjaxSettings' => ['options' => ['id' => 'grid']],
                        // 'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'class' => 'yii\grid\SerialColumn',
                            'headerOptions' => ['class' => 'text-right'],
                            'contentOptions' => ['class' => 'text-right'],
                        ],
                            // 'id',
                        [
                            'attribute' => 'training_participant_id',
                            'value' => 'trainingParticipant.name',
                            'filterType' => GridView::FILTER_SELECT2,
                            'filter' => ArrayHelper::map(TrainingParticipant::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                            'filterInputOptions' => ['placeholder' => ''],
                            'filterWidgetOptions' => [
                                'pluginOptions' => ['allowClear' => true],
                            ],
                        ],
                        'is_approved:integer',
                        [
                            'attribute' => 'remark',
                            'format' => 'ntext',
                            'contentOptions' => ['class' => 'text-wrap'],
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>

    </div>
</div>
