<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ChecksheetEquipmentDumping */

$this->title = 'Create Checksheet Equipment Dumping';
$this->params['breadcrumbs'][] = ['label' => 'Checksheet Equipment Dumping', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="checksheet-equipment-dumping-create box-- box-success--">
	<!-- <div class="box-header"></div> -->

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>
    
</div>
