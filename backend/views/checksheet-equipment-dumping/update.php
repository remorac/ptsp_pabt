<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ChecksheetEquipmentDumping */

$this->title = 'Update Checksheet Equipment Dumping: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Checksheet Equipment Dumping', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="checksheet-equipment-dumping-update box-- box-warning--">

    <!-- <div class="box-header"></div> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
