<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SolarUsageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="solar-usage-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'equipment_id') ?>

    <?= $form->field($model, 'solar_truck_name') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'shift') ?>

    <?php // echo $form->field($model, 'volume') ?>

    <?php // echo $form->field($model, 'hourmeter') ?>

    <?php // echo $form->field($model, 'flowmeter_before') ?>

    <?php // echo $form->field($model, 'flowmeter_after') ?>

    <?php // echo $form->field($model, 'operator_equipment') ?>

    <?php // echo $form->field($model, 'operator_solar_truck') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
