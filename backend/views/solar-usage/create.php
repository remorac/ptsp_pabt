<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\SolarUsage */

$this->title = 'Create Solar Usage';
$this->params['breadcrumbs'][] = ['label' => 'Solar Usage', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="solar-usage-create box-- box-success--">
	<!-- <div class="box-header"></div> -->

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>
    
</div>
