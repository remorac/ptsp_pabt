<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\SolarUsage */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Solar Usage', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="solar-usage-view box-- box-info--">

    <div class="box-body--">
        <p>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> '. 'Update', ['update', 'id' => $model->id], [
            'class' => 'btn btn-warning',
        ]) ?>
        <?= Html::a('<i class="glyphicon glyphicon-trash"></i> ' . 'Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> ' . 'Create', ['create'], [
            'class' => 'btn btn-success',
        ]) ?>
        </p>

        <div class="detail-view-container">
        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                // 'id',
                'equipment.name:text:Equipment',
                'solar_truck_name',
                'date',
                'shift:integer',
                [
                    'attribute' => 'volume',
                    'format' => ['decimal', 2],
                ],
                [
                    'attribute' => 'hourmeter',
                    'format' => ['decimal', 2],
                ],
                'flowmeter_before:integer',
                'flowmeter_after:integer',
                'operator_equipment',
                'operator_solar_truck',
                // 'created_at:datetime',
                // 'updated_at:datetime',
                // 'createdBy.username:text:Created By',
                // 'updatedBy.username:text:Updated By',
            ],
        ]) ?>
        </div>
    </div>
</div>
