<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\SolarUsage */

$this->title = 'Update Solar Usage: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Solar Usage', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="solar-usage-update box-- box-warning--">

    <!-- <div class="box-header"></div> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
