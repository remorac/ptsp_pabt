<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use backend\models\Equipment;

/* @var $this yii\web\View */
/* @var $model backend\models\SolarUsage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="solar-usage-form">

<div class="row">
<div class="col-md-8 col-sm-12">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
        'readonly' => true,
        'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
    ]); ?>

    <?= $form->field($model, 'equipment_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Equipment::find()->all(), 'id', 'name'),
        'options' => ['placeholder' => ''],
        'pluginOptions' => ['allowClear' => true],
    ]); ?>

    <?= $form->field($model, 'shift')->textInput() ?>

    <?= $form->field($model, 'hourmeter')->textInput() ?>

    <?= $form->field($model, 'volume')->textInput() ?>

    <?= $form->field($model, 'solar_truck_name')->radioList([
        'MS13' => 'MS13',
        'MS15' => 'MS15',
        'MS18' => 'MS18',
        'MS19' => 'MS19',
        'MS20' => 'MS20',
    ]) ?>

    <?= $form->field($model, 'flowmeter_before')->textInput() ?>

    <?= $form->field($model, 'flowmeter_after')->textInput() ?>

    <?= $form->field($model, 'operator_equipment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'operator_solar_truck')->textInput(['maxlength' => true]) ?>

    
    <div class="form-panel">
        <div class="row">
    	    <div class="col-sm-12">
    	        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . ($model->isNewRecord ? 'Create' : 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
	    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>

</div>
