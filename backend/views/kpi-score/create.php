<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\KpiScore */

$this->title = Yii::t('app', 'Create Kpi Score');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kpi Scores'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpi-score-create box box-success">
	<div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
