<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\bootstrap\ActiveForm;
use miloschuman\highcharts\Highcharts;
use backend\models\Tire;
use backend\models\TireUsage;
use Box\Spout\Writer\Style\Style;
use backend\models\Hourmeter;

/* @var $this yii\web\View */

$this->title = 'Pemakaian Ban Alat Berat Tambang ' . $year;
// print("<pre>"); 
$months = [];
$month_numbers = [];
for ($i = 1; $i <=12 ; $i++) { 
    $months[] = date('M', strtotime(date('Y-'.$i.'-d')));
    $month_numbers[date('M', strtotime(date('Y-'.$i.'-d')))] = $i;
}
$options = [];
$available_years = TireUsage::find()->select('year(date) as year')->groupBy('year(date)')->asArray()->all();
foreach ($available_years as $available_year) {
    $options = array_replace($options, [$available_year['year'] => $available_year['year']]);
}
// print_r($options);
?>

<div class="site-index">
    <?= Html::textInput('year', $year, ['class' => 'form-control pull-right', 'style' => 'display:block; width:100px', 'onchange' => 'javascript:window.location="'. Url::to(['/site/index']) .'&year="+this.value']) ?>

    <div id="chart">

        <?php
        $categories = [];
        $series = [];
        $colors = ['green', 'yellow', 'red'];
        $tires = Tire::find()->all();
        // krsort($tires);
        foreach ($tires as $tire) {
            $data = [];
            $categories = [];
           
            foreach ($months as $month) {
                $query = TireUsage::find()->where(['month(date)' => $month_numbers[$month], 'year(date)' => $year, 'tire_id' => $tire->id]);
                // echo $query->createCommand(); die();
                $intval = $query->count();
                // echo $month . ':' . $intval.' -- ';
                
                $categories[] = $month;
                $data[] = [
                    'y' => ($intval ? intval($intval) : null),
                    'url' => Url::to([
                        'tire-usage/index',
                        'TireUsageSearch[tire_id]' => $tire->id,
                    ])
                ];
            }
            $series[] = [
                'name' => $tire->name,
                'data' => $data,
                // 'color' => $colors[($key - 1)],
                'edgeColor' => 'white',
                'edgeWidth' => 0.1,
                'dataLabels' => [
                    'enabled' => true,
                    'format' => '{point.y:.0f}',
                ],
            ];
        }
        ?>

        <?= Highcharts::widget([
            'scripts' => [
                // 'modules/exporting',
                // 'highcharts-3d',
                'themes/grid-light',
            ],
            'options' => [
                'credits' => ['enabled' => false],
                'chart' => [
                    'type' => 'column',
                    /* 'options3d' => [
                        'enabled' => true,
                        'alpha' => 15,
                        'beta' => 15,
                        'depth' => 40,
                        'viewDistance' => 35
                    ], */
                    'style' => [
                        'fontFamily' => 'Segoe UI, Helvetica, Arial, sans-serif',
                        'width' => '100%'
                    ],
                ],
                'title' => ['text' => ''],
                'xAxis' => [
                    'categories' => $categories,
                    // 'labels' => [
                    //     'skew3d' => true,
                    // ]
                ],
                'yAxis' => [
                    'title' => ['text' => ''],
                    'allowDecimals' => false,
                    // 'labels' => [
                    //     'skew3d' => true,
                    // ]
                ],
                'series' => $series,
                'plotOptions' => [
                    'column' => [
                        'stacking' => 'normal',
                        'depth' => 35,
                    ],
                    'series' => [
                        'cursor' => 'pointer',
                        'point' => [
                            'events' => [
                                'click' => new JsExpression('function() {
                                    window.open(this.options.url);
                                }')
                            ]
                        ]
                    ]
                ],
                'legend' => ['reversed' => true]
            ]
        ]); ?>

    </div>
</div>
    

<?php
$js = 
<<<JAVASCRIPT
    initializeClock();
    
    function initializeClock() {
        function updateClock() {
            $(Highcharts.charts).each(function(i,chart){
                chart.reflow(); 
            });
            $(window).resize();
            console.log("window resized");
            clearInterval(timeinterval);
        }
        updateClock();
        var timeinterval = setInterval(updateClock, 1000);
    }
JAVASCRIPT;

$this->registerJs($js, \yii\web\VIEW::POS_READY);
?>


<br>
<h3>Equipments Hourmeter</h3>
<?php 
$dataProvider = new \yii\data\ActiveDataProvider([
    'query' => Hourmeter::find()->select('equipment, max(hourmeter) as hourmeter, date')->where(['is not', 'hourmeter', null])->andWhere(['owner' => 'SP'])->groupBy('equipment'),
    'pagination' => false,
    'sort' => [
        'defaultOrder' => [
            'equipment' => SORT_ASC,
        ]
    ],
]);

echo \kartik\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'pjax' => true,
    'hover' => true,
    'striped' => false,
    'bordered' => false,
    'summary' => false,
    'panel' => false,
    'pjaxSettings' => ['options' => ['id' => 'grid']],
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['class' => 'text-right'],
            'contentOptions' => ['class' => 'text-right'],
        ],
        'equipment',
        [
            'attribute' => 'hourmeter',
            'format' => ['decimal', 2],
            'headerOptions' => ['class' => 'text-right'],
            'contentOptions' => ['class' => 'text-right'],
        ],
        [
            'attribute' => 'date',
            'format' => 'date',
            'headerOptions' => ['class' => 'text-right'],
            'contentOptions' => ['class' => 'text-right'],
        ],
    ],
]);
?>