<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use backend\models\Kpi;
use backend\models\KpiFile;
use backend\models\KpiScore;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\KpiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// Yii::$app->params['showTitle'] = false;
// $this->title = \backend\models\Config::findOne(['name' => 'Dashboard Title'])->value;
$this->title = 'KPI';

$kpis = Kpi::find()->select(['*', '(select count(kpi_score.id) from kpi_score where kpi_id = kpi.id) as count_score'])
    ->where(['kpi.is_excluded' => '0', /* 'is_active' => '1', */ 'kpi.year' => $year])->groupBy('kpi.id')->orderBy('count_score DESC, name ASC')->all();
$kpiExcludeds = Kpi::findAll(['is_excluded' => 1, /* 'is_active' => '1', */ 'year' => $year]);

$years = Kpi::find()->groupBy('year')->all();
?>

<!-- <h1 style="font-size:24px;margin:0; margin-bottom:15px">
    <?= Html::encode($this->title) ?>
    <?= Html::dropDownList('year', $year, ArrayHelper::map($years, 'year', 'year'), ['id' => 'year', 'class' => 'form-control year-selector', 'placeholder' => 'year...', 'style' => 'border-color: #00acd6; font-size: 16px; padding: 2px 8px; margin-top: -5px;']) ?>
</h1> -->

<!-- <div class="visible-xs" style="width:150px;">
    <?= Html::dropDownList('year', $year, ArrayHelper::map($years, 'year', 'year'), ['id' => 'year', 'class' => 'form-control year-selector', 'placeholder' => 'year...', 'style' => 'border-color: #00acd6; font-size: 16px; padding: 2px 8px; margin-top: -5px;']) ?>
    <p></p>
</div> -->

<!-- <div class="box box-primary box-body"> -->
<div class="row">

    <?php if ($kpiExcludeds) { ?> <div class="col-md-12"> <?php } else { ?> <div class="col-md-12"> <?php } ?>
        <div class="box box-primary box-body">

            <table style="width:100%;margin-bottom:5px;">
                <tr>
                    <td style="padding:2px">
                        <?= Html::dropDownList('year', $year, ArrayHelper::map($years, 'year', 'year'), ['id' => 'year', 'class' => 'form-control year-selector', 'placeholder' => 'year...', 'style' => 'width: 150px; border-color: #00acd6; font-size: 16px; padding: 2px 8px; margin-top: -5px;']) ?>
                    </td>
                    <td style="padding:2px" class="text-right">
                        <?= Html::a('<i class="fa fa-gear"></i> Setting', ['/kpi/index'], ['class' => 'btn btn-default', 'style' => 'margin-right:5px']) ?>
                        <?php // echo Html::a('<i class="fa fa-th-list"></i> View Monitoring (Non KPI)', ['/site/index-excluded'], ['class' => 'btn btn-info']) ?>
                    </td>
                </tr>
            </table>

            <div style="width:100%; overflow-y:auto;">
            <table class="table table-bordered table-striped" style="margin-bottom:0;">
            <tr style="background:#0d5884; color:#eee">
                <th style="background:none; border-left-color:#0d5884; vertical-align:middle" rowspan="2" colspan="2" class="text-center"><big><big>K P I</big></big></th>
                <th style="background:none; border-right-color:#0d5884;" colspan="12" class="text-center">Aktual <?= date('Y') ?></th>
            </tr>
            <tr style="background:#0d5884; color:#eee">
                <?php for ($i = 1; $i <= 12; $i++) { 
                    $border_right_color = $i == 12 ? "#0d5884" : "#ddd";
                ?>
                   <!-- <th style="background:none; font-family:monospace; width:1px; white-space:nowrap"><?= str_pad($i, 2, "0", STR_PAD_LEFT); ?></th> -->
                   <th style="background:none; font-family:monospace; width:1px; white-space:nowrap; border-right-color: <?= $border_right_color ?>"><?= date('M', mktime(0, 0, 0, $i, 10)) ?></th>
                <?php } ?>                
            </tr>
            <?php foreach ($kpis as $kpi) { ?>
                <tr>
                    <td style="width:1px; white-space:nowrap; border-right: none">
                        <?= Html::a('<i class="fa fa-calendar text-info"></i>', ['/kpi/view-readonly', 'id' => $kpi->id], ['class' => 'btn btn-default btn-xs']) ?>
                        <?= $kpi->kpiFiles && $kpi->kpiFiles[0]->excel ? Html::a('<i class="fa fa-file-excel-o text-success"></i>', ['/kpi/download', 'id' => $kpi->kpiFiles[0]->id, 'filename' => $kpi->kpiFiles[0]->excel], ['class' => 'btn btn-flat btn-default btn-xs']) : '' ?>
                    </td>
                    <td style="border-left: none; border-right: none">
                        <?= $kpi->kpiFiles && $kpi->kpiFiles[0]->pdf ? Html::a($kpi->name, ['/kpi/download', 'id' => $kpi->kpiFiles[0]->id, 'filename' => $kpi->kpiFiles[0]->pdf]) : $kpi->name ?>
                        <small class="text-muted">
                        <small class="text-muted">
                        <br>
                        <!-- Target:  --><b><?= $kpi->target ? Yii::$app->formatter->asDecimal($kpi->target, 0) : '-' ?></b> <?= $kpi->target ? $kpi->base_unit : ''; ?>
                        | <!-- Polarisasi:  --><b><?= $kpi->polarization ? $kpi->polarization : '-'; ?></b>
                        | <!-- Periode:  --><b><?= $kpi->period_type ? $kpi::periodType($kpi->period_type) : '-'; ?></b>
                        </small>
                        </small>
                    </td>
                    <?php for ($i = 1; $i <= 12; $i++) { 
                        $kpiScore = KpiScore::findOne(['kpi_id' => $kpi->id, 'month' => $i]);
                        $actual = $kpiScore ? $kpiScore->value : null;
                        $background = '';
                        if ($actual) {
                            //if ($actual == $kpi->target) $background = 'yellow';
                            if ($actual == $kpi->target) $background = 'yellow';
                            if ($kpi->polarization == "Max") {
                                if ($actual >= $kpi->target) $background = '#a6e22e';
                                if ($actual < $kpi->target) $background = '#f85b93';
                            }
                            if ($kpi->polarization == "Min") {
                                if ($actual > $kpi->target) $background = '#f85b93';
                                if ($actual <= $kpi->target) $background = '#a6e22e';
                            }
                        }
                        if (!$kpi->is_active) $background = "#aaa";
						$actual_disp = ctype_digit($actual) ? Yii::$app->formatter->asDecimal($actual, 0) : $actual;
                    ?>
                        <td style="background:<?= $background ?>; text-align:center; font-weight:bold; font-family:monospace; border-left:1px solid #ddd; border-right:1px solid #ddd;"><?= $actual_disp ?></td>
                    <?php } ?>
                </tr>
            <?php } ?>
            </table>
            </div>
        </div>
    </div>

    <?php if ($kpiExcludeds) { ?>
    <div class="col-md-12" style="">
        <table class="table table-hover" style="border:1px solid #ddd; background:#fff">
        <?php foreach ($kpiExcludeds as $kpiExcluded) { ?>
            <tr>
                <td style="border-top:1px solid #ddd; width:1px; white-space:nowrap">
                    <?= Html::a('<i class="fa fa-calendar text-info"></i>', ['/kpi/view-readonly', 'id' => $kpiExcluded->id], ['class' => 'btn btn-default btn-xs']) ?>
                    <?= $kpiExcluded->kpiFiles && $kpiExcluded->kpiFiles[0]->excel ? Html::a('<i class="fa fa-file-excel-o text-success"></i>', ['/kpi/download', 'id' => $kpiExcluded->kpiFiles[0]->id, 'filename' => $kpiExcluded->kpiFiles[0]->excel], ['class' => 'btn btn-flat btn-default btn-xs']) : '' ?>
                </td>
                <td style="border-top:1px solid #ddd;"><?= $kpiExcluded->kpiFiles && $kpiExcluded->kpiFiles[0]->pdf ? Html::a($kpiExcluded->name, ['/kpi/download', 'id' => $kpiExcluded->kpiFiles[0]->id, 'filename' => $kpiExcluded->kpiFiles[0]->pdf]) : $kpiExcluded->name ?></td>
            </tr>
        <?php } ?>
        </table>
    </div>
    <?php } ?>

</div>

<!-- </div> -->

<?php 
    $this->registerJs('
        $(".year-selector").change(function() {
            year = $(this).val();
            window.location = "'.Url::base().'?r=site/index&year="+year;
        });
    ', \yii\web\View::POS_READY);
?>