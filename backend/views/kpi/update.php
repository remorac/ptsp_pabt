<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Kpi */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'KPI',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'KPI'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="kpi-update box-- box---warning">

    <div class="box---header"></div>

    <div class="box---body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
