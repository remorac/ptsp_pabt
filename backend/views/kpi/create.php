<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Kpi */

$this->title = Yii::t('app', 'Create KPI');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'KPI'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpi-create box-- box---success">
	<div class="box---header"></div>

    <div class="box---body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
