<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use kartik\widgets\DatePicker;
use kartik\widgets\FileInput;
use backend\models\KpiScore;

/* @var $this yii\web\View */
/* @var $model backend\models\Kpi */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'KPI'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">

    <div class="col-md-6">

        <div class="kpi-view box-- box---info">

            <div class="box---body">
                <p>
                <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> '. Yii::t('app', 'Update'), ['update', 'id' => $model->id], [
                    'class' => 'btn btn-warning',
                ]) ?>
                <?= Html::a('<i class="glyphicon glyphicon-trash"></i> ' . Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
                </p>

                <?= DetailView::widget([
                    'options' => ['class' => 'table detail-view'],
                    'model' => $model,
                    'attributes' => [
                        // 'id',
                        'name',
                        'year',
                        'target',
                        'base_unit',
                        'polarization',
                        [
                            'attribute' => 'period_type',
                            'value' => $model::periodType($model->period_type),
                        ],
                        [
                            'attribute' => 'is_excluded',
                            'value' => $model::isExcluded($model->is_excluded),
                        ],
                        [
                            'attribute' => 'is_active',
                            'value' => $model::isActive($model->is_active),
                        ],
                        'created_at:datetime',
                        'updated_at:datetime',
                        'createdBy.username:text:Created By',
                        'updatedBy.username:text:Updated By',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
        

    <div class="col-md-3">
        <div class="box-- box---info">
            <div class="box---header">
                <div class="box---title">File</div>
            </div>
            <div class="box---body">
                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                <table width="100%">
                    <tr>
                        <td style="vertical-align:top">
                            <?= $form->field($modelFile, 'date')->widget(DatePicker::classname(), [
                                'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                                'readonly' => true,
                                'pluginOptions' => [
                                    'autoclose' => true, 'format' => 'yyyy-mm-dd',
                                ],
                                'options' => [
                                    'placeholder' => 'document date...',
                                ],
                            ])->label(false) ?>
                        </td>    
                    </tr>
                    <tr>
                        <td sstyle="vertical-align:top; padding-left:10px">
                            <?= $form->field($modelFile, 'excelFile')->widget(FileInput::classname(), [
                                'options' => [
                                    'accept' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                                ],
                                'pluginOptions' => [
                                    'showPreview' => false,
                                    'showCaption' => true,
                                    'showRemove' => true,
                                    'showUpload' => false,
                                    'allowedFileExtensions' => ['xlsx',' XLSX',' xls',' XLS',' ods',' ODS'],
                                    // 'maxFileSize'=>8192,
                                    'initialCaption' => 'Excel File...',
                                ]
                            ])->label(false) ?>
                        </td>
                    </tr>
                    <tr>
                        <td sstyle="vertical-align:top; padding-left:10px">
                            <?= $form->field($modelFile, 'pdfFile')->widget(FileInput::classname(), [
                                'options' => [
                                    'accept' => 'application/pdf',
                                ],
                                'pluginOptions' => [
                                    'showPreview' => false,
                                    'showCaption' => true,
                                    'showRemove' => true,
                                    'showUpload' => false,
                                    'allowedFileExtensions' => ['pdf', 'PDF'],
                                    // 'maxFileSize'=>8192,
                                    'initialCaption' => 'PDF File...',
                                ]
                            ])->label(false) ?>
                        </td>
                    </tr>
                    <tr>
                        <td sstyle="vertical-align:top; padding-left:10px"><?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . ($modelFile->isNewRecord ? Yii::t('app', 'Add') : Yii::t('app', 'Update')), ['class' => $modelFile->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?></td>    
                    </tr>
                </table>
                <?php ActiveForm::end(); ?>
                <br>

                <?= GridView::widget([
                    'dataProvider' => $dataProviderFile,
                    // 'pjax' => true,
                    'hover' => true,
                    'striped' => false,
                    'bordered' => false,
                    'panel' => false,
                    'pjaxSettings' => ['options' => ['id' => 'gridFile']],
                    'columns' => [
                        [
                            'contentOptions' => ['class' => 'action-column nowrap text-left'],
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{delete}',
                            'buttons' => [
                                'delete' => function ($url, $model) {
                                    return Html::a('', ['/kpi/delete-file', 'id' => $model->id], [
                                        'class' => 'glyphicon glyphicon-trash btn btn-xs btn-danger', 
                                        'data-method' => 'post', 
                                        'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?')]);
                                },
                            ],
                        ],
                        'date:date:Document Date',
                        [
                            'header' => 'Excel',
                            'format'=> 'html',
                            'value' => function($data) {
                                return ($data->excel ? Html::a('<i class="fa fa-file-excel-o text-success"></i>', ['/kpi/download', 'id' => $data->id, 'filename' => $data->excel], ['class' => 'btn btn-flat btn-default btn-xs', 'data-pjax' => 0]) : '');
                            },
                            'contentOptions' => ['style' => 'width:1px; text-align:right; white-space:nowrap'],
                        ],
                        [
                            'header' => 'PDF',
                            'format'=> 'html',
                            'value' => function($data) {
                                return ($data->pdf ? Html::a('<i class="fa fa-file-pdf-o text-danger"></i>', ['/kpi/download', 'id' => $data->id, 'filename' => $data->pdf], ['class' => 'btn btn-flat btn-default btn-xs', 'data-pjax' => 0]) : '');
                            },
                            'contentOptions' => ['style' => 'width:1px; text-align:right; white-space:nowrap'],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
    
    <?php if (!$model->is_excluded) { ?>
    <div class="col-md-3">
        <div class="box-- box---info">
            <div class="box---header">
                <div class="box---title">Realisasi</div>
            </div>
            <div class="box---body">
                <?php $form = ActiveForm::begin(); ?>
                <table width="100%">
                    <tr>
                        <td sstyle="vertical-align:top; padding-left:10px"><?= $form->field($modelScore, 'month')->dropDownList($modelScore::month('all'), ['prompt' => 'month...'])->label(false) ?></td>    
                    </tr>
                    <tr>
                        <td sstyle="vertical-align:top; padding-left:10px"><?= $form->field($modelScore, 'value')->textInput(['placeholder' => 'value'])->label(false) ?></td>    
                    </tr>
                    <tr>
                        <td sstyle="vertical-align:top; padding-left:10px"><?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . ($modelScore->isNewRecord ? Yii::t('app', 'Add') : Yii::t('app', 'Update')), ['class' => $modelScore->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?></td>    
                    </tr>
                </table>
                <?php ActiveForm::end(); ?>
                <br>

                <?= GridView::widget([
                    'dataProvider' => $dataProviderScore,
                    // 'pjax' => true,
                    'hover' => true,
                    'striped' => false,
                    'bordered' => false,
                    'panel' => false,
                    'pjaxSettings' => ['options' => ['id' => 'gridScore']],
                    'columns' => [
                        [
                            'contentOptions' => ['class' => 'action-column nowrap text-left'],
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{delete}',
                            'buttons' => [
                                'delete' => function ($url, $model) {
                                    return Html::a('', ['/kpi/delete-score', 'id' => $model->id], [
                                        'class' => 'glyphicon glyphicon-trash btn btn-xs btn-danger', 
                                        'data-method' => 'post', 
                                        'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?')]);
                                },
                            ],
                        ],
                        [
                            'attribute' => 'month',
                            'value' => function($data) { return KpiScore::month($data->month); }
                        ],
                        [
                            'attribute' => 'value',
                            'headerOptions' => ['style' => 'width:1px; text-align:right'],
                            'contentOptions' => ['style' => 'width:1px; text-align:right'],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
    <?php } ?>

</div>