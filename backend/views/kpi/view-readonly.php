<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use backend\models\KpiScore;

/* @var $this yii\web\View */
/* @var $model backend\models\Kpi */

$this->title = $model->name;
// $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'KPI'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-6">

        <div class="kpi-view box-- box---info">

            <div class="box---body">
                <p>
                <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> '. Yii::t('app', 'Update'), ['view', 'id' => $model->id], [
                    'class' => 'btn btn-warning',
                ]) ?>
                </p>

                <?= DetailView::widget([
                    'options' => ['class' => 'table detail-view'],
                    'model' => $model,
                    'attributes' => [
                        // 'id',
                        'name',
                        'year',
                        'target',
                        'base_unit',
                        'polarization',
                        'period_type',
                        // 'is_excluded:integer',
                        // 'is_active:integer',
                        // 'created_at:datetime',
                        // 'updated_at:datetime',
                        // 'createdBy.username:text:Created By',
                        // 'updatedBy.username:text:Updated By',
                    ],
                ]) ?>
            </div>
        </div>
    </div>



    <div class="col-md-3">
        <div class="box-- box---info">
            <div class="box---header">
                <div class="box---title">File</div>
            </div>
            <div class="box---body">
                <?= GridView::widget([
                    'dataProvider' => $dataProviderFile,
                    // 'pjax' => true,
                    'hover' => true,
                    'striped' => false,
                    'bordered' => false,
                    'panel' => false,
                    'summary' => false,
                    'pjaxSettings' => ['options' => ['id' => 'gridFile']],
                    'columns' => [
                        'date:date:Document Date',
                        [
                            'header' => 'Excel',
                            'format'=> 'html',
                            'value' => function($data) {
                                return ($data->excel ? Html::a('<i class="fa fa-file-excel-o text-success"></i>', ['/kpi/download', 'id' => $data->id, 'filename' => $data->excel], ['class' => 'btn btn-flat btn-default btn-xs', 'data-pjax' => 0]) : '');
                            },
                            'headerOptions' => ['style' => 'width:1px; text-align:right; white-space:nowrap'],
                            'contentOptions' => ['style' => 'width:1px; text-align:right; white-space:nowrap'],
                        ],
                        [
                            'header' => 'PDF',
                            'format'=> 'html',
                            'value' => function($data) {
                                return ($data->pdf ? Html::a('<i class="fa fa-file-pdf-o text-danger"></i>', ['/kpi/download', 'id' => $data->id, 'filename' => $data->pdf], ['class' => 'btn btn-flat btn-default btn-xs', 'data-pjax' => 0]) : '');
                            },
                            'headerOptions' => ['style' => 'width:1px; text-align:right; white-space:nowrap'],
                            'contentOptions' => ['style' => 'width:1px; text-align:right; white-space:nowrap'],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
    
    <?php if (!$model->is_excluded) { ?>
    <div class="col-md-3">
        <div class="box-- box---info">
            <div class="box---header">
                <div class="box---title">Realisasi</div>
            </div>
            <div class="box---body">
                <?= GridView::widget([
                    'dataProvider' => $dataProviderScore,
                    // 'pjax' => true,
                    'hover' => true,
                    'striped' => false,
                    'bordered' => false,
                    'panel' => false,
                    'summary' => false,
                    'pjaxSettings' => ['options' => ['id' => 'gridScore']],
                    'columns' => [
                        [
                            'attribute' => 'month',
                            'value' => function($data) { return KpiScore::month($data->month); }
                        ],
                        [
                            'attribute' => 'value',
                            'headerOptions' => ['style' => 'width:1px; text-align:right'],
                            'contentOptions' => ['style' => 'width:1px; text-align:right'],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
    <?php } ?>

</div>