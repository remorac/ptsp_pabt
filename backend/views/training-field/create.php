<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TrainingField */

$this->title = 'Create Training Field';
$this->params['breadcrumbs'][] = ['label' => 'Training Field', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-field-create box-- box-success--">
	<!-- <div class="box-header"></div> -->

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>
    
</div>
