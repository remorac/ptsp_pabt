<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TireIn */

$this->title = 'Update Tire In: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tire In', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tire-in-update box-- box-warning--">

    <!-- <div class="box-header"></div> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
