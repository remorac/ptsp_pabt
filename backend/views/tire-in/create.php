<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TireIn */

$this->title = 'Create Tire In';
$this->params['breadcrumbs'][] = ['label' => 'Tire In', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tire-in-create box-- box-success--">
	<!-- <div class="box-header"></div> -->

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>
    
</div>
