<?php 
    use yii\helpers\Url;
?>

<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <?php if (!Yii::$app->user->isGuest) { ?>
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= Url::base().'/img/user.jpg' ?>" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->username ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> <?= Yii::$app->user->identity->email ?></a>
            </div>
        </div>
        <?php } ?>

        <?php   
            $menuItems = [
                ['label' => '<b>MENU</b>', 'encode' => false, 'options' => ['class' => 'header']],
                ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],

                // ['label' => 'Home', 'icon' => 'fa fa-home', 'url' => ['/site/index']],
                [
                    'label' => 'BAN',
                    'icon' => 'fa fa-support',
                    'url' => '#',
                    'options' => ['class' => 'treeview'],
                    'items' => [
                        [
                            'label' => 'Data Setting',
                            'url' => '#',
                            'options' => ['class' => 'treeview'],
                            'items' => [
                                ['label' => 'Alat Berat', 'url' => ['/equipment/index']],
                                ['label' => 'Ban', 'url' => ['/tire/index']],
                            ],
                        ],
                        ['label' => 'Pengambilan', 'url' => ['/tire-in/index']],
                        ['label' => 'Pemakaian', 'url' => ['/tire-usage/index']],
                    ],
                ],
                [
                    'label' => 'TRAINING',
                    'icon' => 'fa fa-users',
                    'url' => '#',
                    'options' => ['class' => 'treeview'],
                    'items' => [
                        [
                            'label' => 'Data Setting',
                            'url' => '#',
                            'options' => ['class' => 'treeview'],
                            'items' => [
                                ['label' => 'Field', 'url' => ['/training-field/index']],
                                ['label' => 'Participant', 'url' => ['/training-participant/index']],
                            ],
                        ],
                        ['label' => 'Event', 'url' => ['/training-event/index']],
                    ],
                ],
                ['label' => 'CHECK SHEET', 'icon' => 'fa fa-files-o', 'url' => ['/checksheet/index']],
                ['label' => 'SOLAR', 'icon' => 'fa fa-truck', 'url' => ['/solar-usage/index']],
                ['label' => 'KPI', 'icon' => 'fa fa-th-list', 'url' => ['/kpi/report']],
                ['label' => 'User', 'icon' => 'fa fa-user', 'url' => ['/user/index']],
                /* [
                    'label' => 'Access Control',
                    'icon' => 'fa fa-lock',
                    'url' => '#',
                    'options' => ['class' => 'treeview'],
                    // 'visible' => Yii::$app->user->can('superuser'),
                    'items' => [
                        ['label' => 'User',         'url' => ['/user/index']],
                        ['label' => 'Assignment',   'url' => ['/acf/assignment']],
                        ['label' => 'Role',         'url' => ['/acf/role']],
                        ['label' => 'Permission',   'url' => ['/acf/permission']],
                        ['label' => 'Route',        'url' => ['/acf/route']],
                        ['label' => 'Rule',         'url' => ['/acf/rule']],
                    ],
                ], */
                // ['label' => 'Log', 'icon' => 'fa fa-clock-o', 'url' => ['/log/index'],'visible' => Yii::$app->user->can('superuser')],
            ];

            // $menuItems = mdm\admin\components\Helper::filter($menuItems);
        ?>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => $menuItems,
            ]
        ) ?>
        
        <!-- <ul class="sidebar-menu"><li><a href="<?= \yii\helpers\Url::to(['site/logout']) ?>" data-method="post"><i class="fa fa-sign-out"></i>  <span>Logout</span></a></li></ul> -->
    
    </section>

</aside>
