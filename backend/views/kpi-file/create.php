<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\KpiFile */

$this->title = Yii::t('app', 'Create Kpi File');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kpi Files'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kpi-file-create box box-success">
	<div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
