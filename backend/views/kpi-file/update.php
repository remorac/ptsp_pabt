<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\KpiFile */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Kpi File',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kpi Files'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="kpi-file-update box box-warning">

    <div class="box-header"></div>

    <div class="box-body">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
    </div>

</div>
