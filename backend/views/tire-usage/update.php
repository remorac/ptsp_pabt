<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TireUsage */

$this->title = 'Update Tire Usage: ' . $model->serial;
$this->params['breadcrumbs'][] = ['label' => 'Tire Usage', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->serial, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tire-usage-update box-- box-warning--">

    <!-- <div class="box-header"></div> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
