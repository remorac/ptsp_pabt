<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TireUsage */

$this->title = 'Create Tire Usage';
$this->params['breadcrumbs'][] = ['label' => 'Tire Usage', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tire-usage-create box-- box-success--">
	<!-- <div class="box-header"></div> -->

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>
    
</div>
