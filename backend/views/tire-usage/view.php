<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\TireUsage */

$this->title = $model->serial;
$this->params['breadcrumbs'][] = ['label' => 'Tire Usage', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="tire-usage-view box-- box-info--">

    <div class="box-body--">
        <p>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> '. 'Update', ['update', 'id' => $model->id], [
            'class' => 'btn btn-warning',
        ]) ?>
        <?= Html::a('<i class="glyphicon glyphicon-trash"></i> ' . 'Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        </p>

        <div class="detail-view-container">
        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                // 'id',
                'equipment.name:text:Equipment',
                'tire.name:text:Tire',
                'serial',
                'date',
                'hourmeter:integer',
                'cause',
                'position',
                'is_first_usage:integer',
                'remark',
                // 'created_at:datetime',
                // 'updated_at:datetime',
                // 'createdBy.username:text:Created By',
                // 'updatedBy.username:text:Updated By',
            ],
        ]) ?>
        </div>
    </div>
</div>

<div class="detail-view-container">
<table class="table table-hover detail-view">
<?php 
$path = Yii::getAlias('@uploads/tire-usage/' . $model->id . '/');

$files = [];
$unsorted_files = file_exists($path) ? new \DirectoryIterator($path) : [];

foreach ($unsorted_files as $file) {
    $files[] = [
        'sort_filename' => strtolower($file->getFilename()),
        'filename' => $file->getFilename(),
        'isDir' => $file->isDir(),
        'isDot' => $file->isDot(),
    ];
}
asort($files);
foreach ($files as $file) {
    if (!$file['isDot'] && !$file['isDir']) {
        $is_empty = 0;
        ?>
        <tr>
            <!-- <td style="vertical-align: top" width="1px"><big><i class="fa fa-file text-success"></i></big></td> -->
            <td style="vertical-align: top" width="50%"><img style="max-width:100%" src="<?= Url::to(['download', 'id' => $model->id, 'filename' => $file['filename']]) ?>"></big></td>
            <!-- <td style="vertical-align: top" width="1px">
                <?= Html::a('<i class="fa fa-remove text-danger"></i>', ['remove', 'id' => $model->id, 'filename' => $file['filename']], [
                    'class' => 'btn btn-xs btn-default btn-text-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            </td> -->
            <td style="vertical-align: top"><?= Html::a($file['filename'], ['download', 'id' => $model->id, 'filename' => $file['filename']]) ?></td>
        </tr>
    <?php 
    }
}
?>
</table>
</div>