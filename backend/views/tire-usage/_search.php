<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TireUsageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tire-usage-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'equipment_id') ?>

    <?= $form->field($model, 'tire_id') ?>

    <?= $form->field($model, 'serial') ?>

    <?= $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'hourmeter') ?>

    <?php // echo $form->field($model, 'cause') ?>

    <?php // echo $form->field($model, 'position') ?>

    <?php // echo $form->field($model, 'is_first_usage') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
