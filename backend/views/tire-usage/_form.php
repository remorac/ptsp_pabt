<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;
use kartik\widgets\FileInput;
use backend\models\Equipment;
use backend\models\Tire;

/* @var $this yii\web\View */
/* @var $model backend\models\TireUsage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-md-8 col-sm-12">

        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <?= $form->field($model, 'equipment_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Equipment::find()->all(), 'id', 'name'),
            'options' => ['placeholder' => ''],
            'pluginOptions' => ['allowClear' => true],
        ]); ?>

        <?= $form->field($model, 'tire_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Tire::find()->all(), 'id', 'name'),
            'options' => ['placeholder' => ''],
            'pluginOptions' => ['allowClear' => true],
        ]); ?>

        <?= $form->field($model, 'serial')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
            'type' => DatePicker::TYPE_COMPONENT_PREPEND,
            'readonly' => true,
            'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
        ]); ?>

        <?= $form->field($model, 'hourmeter')->textInput() ?>

        <?= $form->field($model, 'cause')->dropDownList(['Tipis Terbenang' => 'Tipis Terbenang', 'Bocor/Accident' => 'Bocor/Accident']) ?>

        <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
            <label class="control-label">File</label>
            <div class="row">
            <div class="col-md-12">
                <?php 
                echo FileInput::widget([
                    'name' => 'files[]',
                    'options' => [
                        // 'accept' => 'application/pdf',
                        'multiple' => true,
                    ],
                    'pluginOptions' => [
                        'showPreview' => true,
                        'showCaption' => true,
                        'showRemove' => true,
                        'showUpload' => true,
                        // 'allowedFileExtensions' => ['pdf'],
                        // 'maxFileSize' => 3072000,
                        // 'maxFileCount' => 20,
                    ],
                ]);
                ?>
            </div>
            </div>
        </div>

        <?= $form->field($model, 'remark')->textarea(['rows' => 3]) ?>

        <div class="form-panel">
            <div class="row">
                <div class="col-sm-12">
                    <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . ($model->isNewRecord ? 'Create' : 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
    <div class="col-md-4">

    <?php if (!$model->isNewRecord) { ?>
        <div class="detail-view-container">
            <table class="table table-hover detail-view">
            <?php 
            $path = Yii::getAlias('@uploads/tire-usage/' . $model->id . '/');

            $files = [];
            $unsorted_files = file_exists($path) ? new \DirectoryIterator($path) : [];

            foreach ($unsorted_files as $file) {
                $files[] = [
                    'sort_filename' => strtolower($file->getFilename()),
                    'filename' => $file->getFilename(),
                    'isDir' => $file->isDir(),
                    'isDot' => $file->isDot(),
                ];
            }
            asort($files);
            foreach ($files as $file) {
                if (!$file['isDot'] && !$file['isDir']) {
                    $is_empty = 0;
                    ?>
                    <tr>
                        <!-- <td style="vertical-align: top" width="1px"><big><i class="fa fa-file text-success"></i></big></td> -->
                        <td style="vertical-align: top" width="50%"><img style="max-width:100%" src="<?= Url::to(['download', 'id' => $model->id, 'filename' => $file['filename']]) ?>"></big></td>
                        <td style="vertical-align: top" width="1px">
                            <?= Html::a('<i class="fa fa-remove text-danger"></i>', ['remove', 'id' => $model->id, 'filename' => $file['filename']], [
                                'class' => 'btn btn-xs btn-default btn-text-danger',
                                'data' => [
                                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </td>
                        <td style="vertical-align: top"><?= Html::a($file['filename'], ['download', 'id' => $model->id, 'filename' => $file['filename']]) ?></td>
                    </tr>
                <?php 
            }
            }
            ?>
            </table>
        </div>
    <?php } ?>

    </div>
</div>
