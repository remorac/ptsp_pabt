<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TrainingParticipant */

$this->title = 'Create Training Participant';
$this->params['breadcrumbs'][] = ['label' => 'Training Participant', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-participant-create box-- box-success--">
	<!-- <div class="box-header"></div> -->

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>
    
</div>
