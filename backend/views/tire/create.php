<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Tire */

$this->title = 'Create Tire';
$this->params['breadcrumbs'][] = ['label' => 'Tire', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tire-create box-- box-success--">
	<!-- <div class="box-header"></div> -->

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>
    
</div>
