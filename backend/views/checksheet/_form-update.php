<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\datecontrol\DateControl;

use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use kartik\grid\GridView;
use backend\models\Equipment;
use backend\models\ChecksheetEquipment;

/* @var $this yii\web\View */
/* @var $model backend\models\Incoming */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne" style="border-bottom: none">
        <h4 class="panel-title">
            <?= $model->field .' | Shift '. $model->shift .' | Group '. $model->team . ' | ' . Yii::$app->formatter->asDatetime($model->started_at) . ' - ' . Yii::$app->formatter->asDatetime($model->ended_at) ?>
            <a class="pull-right btn btn-default btn-text-warning btn-xs" style="margin-top:-2px" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                <small><i class="glyphicon glyphicon-pencil text-warning"></i></small>
            </a>
        </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body" style="background: #fdfdfd">
            
            <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

            <?= $form->field($model, 'field')->dropDownList([
                'DRILLING, BLASTING & MINING SERVICES' => 'DRILLING, BLASTING & MINING SERVICES',
                'LOADING & HAULING' => 'LOADING & HAULING',
            ]) ?>

            <?= $form->field($model, 'shift')->dropDownList([
                '1' => '1',
                '2' => '2',
                '3' => '3',
            ]) ?>

            <?= $form->field($model, 'team')->dropDownList([
                'A' => 'A',
                'B' => 'B',
                'C' => 'C',
                'D' => 'D',
            ]) ?>

            <?= $form->field($model, 'started_at')->widget(DateControl::className(), [
                'type' => DateControl::FORMAT_DATETIME,
                'widgetOptions' => [
                    'pluginOptions' => [
                        'autoclose' => true,
                    ]
                ],
            ]) ?>

            <?= $form->field($model, 'ended_at')->widget(DateControl::className(), [
                'type' => DateControl::FORMAT_DATETIME,
                'widgetOptions' => [
                    'pluginOptions' => [
                        'autoclose' => true,
                    ]
                ],
            ]) ?>

            <?= $form->field($model, 'personnel_leave')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'personnel_sick')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'personnel_off')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'personnel_overtime')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'field_information')->textarea(['rows' => 6]) ?>
            
            <div class="form-panel">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
    
<?php $form = ActiveForm::begin([
    /* 'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-3',
            'offset' => 'col-sm-offset-3',
            'wrapper' => 'col-sm-9',
            'error' => '',
            'hint' => '',
        ],
    ], */
]); ?>

<div class="row">
    <div class="col-md-3">
        
        <?= $form->field($modelItem, 'equipment_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Equipment::find()->all(), 'id', 'nameWithLastHourmeter'),
            'options' => ['placeholder' => ''],
            'pluginOptions' => ['allowClear' => true],
        ]); ?>

        <?= $form->field($modelItem, 'operator')->textInput(['maxlength' => true]) ?>

        <?= $form->field($modelItem, 'hourmeter_start')->textInput() ?>

        <?= $form->field($modelItem, 'hourmeter_end')->textInput() ?>

        <?= $form->field($modelItem, 'production')->textInput() ?>

        <?= $form->field($modelItem, 'ritasi')->textInput() ?>

        <?= $form->field($modelItem, 'location')->textInput(['maxlength' => true]) ?>

        <?= $form->field($modelItem, 'interruption_description')->textInput() ?>

        <?= $form->field($modelItem, 'interruption_started_at')->textInput() ?>

        <?= $form->field($modelItem, 'interruption_ended_at')->textInput() ?>

        <?= $form->field($modelItem, 'geometry_hole')->textInput() ?>

        <?= $form->field($modelItem, 'geometry_space')->textInput() ?>

        <?= $form->field($modelItem, 'geometry_burden')->textInput() ?>

        <?= $form->field($modelItem, 'geometry_depth')->textInput() ?>

        <div class="form-panel" style="margin-bottom:20px">
            <div class="row">
                <div class="col-sm-12">
                    <?php 
                    if ($modelItem->isNewRecord) {
                        echo Html::submitButton('<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('app', 'Add'), ['class' => 'btn btn-success pull-right']);
                    } else {
                        echo '<div class="input-group pull-right">';
                        echo Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . Yii::t('app', 'Update'), ['class' => 'btn btn-primary'])
                            . ' ' . Html::a('<i class="glyphicon glyphicon-remove"></i> ' . Yii::t('app', 'Cancel'), ['update', 'id' => $model->id], [
                            'class' => 'btn btn-default delete-item',
                        ]);
                        echo "</div>";
                    }
                    ?>
                </div>
            </div>
        </div>

    </div>
    
    <div class="col-md-9">
        <div class="row">
            <div class="col-sm-12">
                <?php 
                $dataProvider = new \yii\data\ActiveDataProvider([
                    'query' => ChecksheetEquipment::find()->where(['checksheet_id' => $model->id]),
                    'pagination' => false,
                    'sort' => [
                        'defaultOrder' => [
                            'id' => SORT_DESC,
                        ]
                    ],
                ]);

                echo \kartik\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                        // 'pjax' => true,
                    'hover' => true,
                    'striped' => false,
                    'bordered' => false,
                    'summary' => false,
                    'toolbar' => [
                            // Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']),
                        Html::a('<i class="fa fa-repeat"></i> ' . Yii::t('app', 'Reload'), ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default']),
                        '{toggleData}',
                            // $exportMenu,
                    ],
                    'panel' => false,
                    'pjaxSettings' => ['options' => ['id' => 'grid']],
                        // 'filterModel' => $searchModel,
                    // 'tableOptions' => ['class' => 'table table-condensed small'],
                    'rowOptions' => function ($model) use ($modelItem) {
                        return $model->id == $modelItem->id ? ['style' => 'background:#ffff77'] : '';
                    },
                    'columns' => [
                        [
                            'contentOptions' => ['class' => 'action-column nowrap text-right', 'style' => 'padding:3px 5px 0 5px'],
                            'attribute' => '',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return
                                    Html::a('<i class="glyphicon glyphicon-pencil"></i>', ['update', 'id' => $data->checksheet_id, 'checksheet_equipment_id' => $data->id], [
                                    'class' => 'btn btn-xs btn-default btn-text-warning',
                                    'style' => 'padding:0px 5px; margin:0px',
                                ])
                                    . ' ' .
                                    Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete-item', 'id' => $data->id], [
                                    'class' => 'btn btn-xs btn-default btn-text-danger',
                                    'style' => 'padding:0px 5px; margin:0px',
                                    'data-confirm' => 'Are you sure you want to delete this item?',
                                    'data-method' => 'post',
                                ]);
                            }
                        ],
                        [
                            'class' => 'yii\grid\SerialColumn',
                            'headerOptions' => ['class' => 'text-right'],
                            'contentOptions' => ['class' => 'text-right'],
                        ],
                        [
                            'attribute' => 'equipment_id',
                            'value' => 'equipment.name',
                        ],
                        'operator',
                        [
                            'attribute' => 'hourmeter_start',
                            'format' => ['decimal', 2],
                            'headerOptions' => ['class' => 'text-right'],
                            'contentOptions' => ['class' => 'text-right'],
                        ],
                        [
                            'attribute' => 'hourmeter_end',
                            'format' => ['decimal', 2],
                            'headerOptions' => ['class' => 'text-right'],
                            'contentOptions' => ['class' => 'text-right'],
                        ],
                        [
                            'attribute' => 'production',
                            'format' => ['decimal', 2],
                            'headerOptions' => ['class' => 'text-right'],
                            'contentOptions' => ['class' => 'text-right'],
                        ],
                        [
                            'attribute' => 'ritasi',
                            'format' => 'integer',
                            'headerOptions' => ['class' => 'text-right'],
                            'contentOptions' => ['class' => 'text-right'],
                        ],
                        'location',
                        [
                            'attribute' => 'interruption_description',
                        ],
                        [
                            'attribute' => 'interruption_started_at',
                        ],
                        [
                            'attribute' => 'interruption_ended_at',
                        ],
                        [
                            'attribute' => 'geometry_hole',
                            'format' => ['decimal', 2],
                            'headerOptions' => ['class' => 'text-right'],
                            'contentOptions' => ['class' => 'text-right'],
                        ],
                        [
                            'attribute' => 'geometry_space',
                            'format' => ['decimal', 2],
                            'headerOptions' => ['class' => 'text-right'],
                            'contentOptions' => ['class' => 'text-right'],
                        ],
                        [
                            'attribute' => 'geometry_burden',
                            'format' => ['decimal', 2],
                            'headerOptions' => ['class' => 'text-right'],
                            'contentOptions' => ['class' => 'text-right'],
                        ],
                        [
                            'attribute' => 'geometry_depth',
                            'format' => ['decimal', 2],
                            'headerOptions' => ['class' => 'text-right'],
                            'contentOptions' => ['class' => 'text-right'],
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>



<div class="form-panel">
    <div class="row">
        <div class="col-sm-12">
            <?= Html::a('<i class="glyphicon glyphicon-stop"></i> '. Yii::t('app', 'Done'), ['view', 'id' => $model->id], [
                'class' => 'btn btn-default btn-text-danger pull-right',
            ]) ?>
        </div>
    </div>
</div>



<?php 
    $this->registerJsFile(
        '@web/js/incoming-purchase.js',
        ['depends' => [\yii\web\JqueryAsset::className()]]
    );
?>