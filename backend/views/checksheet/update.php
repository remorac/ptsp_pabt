<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Checksheet */

$this->title = 'Update Checksheet: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Checksheet', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="checksheet-update box-- box-warning--">

    <!-- <div class="box-header"></div> -->

    <?= $this->render('_form-update', [
        'model' => $model,
        'modelItem' => $modelItem,
    ]) ?>

</div>
