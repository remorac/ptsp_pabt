<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model backend\models\Checksheet */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="checksheet-form">

<div class="row">
<div class="col-md-8 col-sm-12">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'field')->dropDownList([
        'DRILLING, BLASTING & MINING SERVICES' => 'DRILLING, BLASTING & MINING SERVICES',
        'LOADING & HAULING' => 'LOADING & HAULING',
    ]) ?>

    <?= $form->field($model, 'shift')->dropDownList([
        '1' => '1',
        '2' => '2',
        '3' => '3',
    ]) ?>

    <?= $form->field($model, 'team')->dropDownList([
        'A' => 'A',
        'B' => 'B',
        'C' => 'C',
        'D' => 'D',
    ]) ?>

    <?= $form->field($model, 'started_at')->widget(DateControl::className(), [
        'type' => DateControl::FORMAT_DATETIME,
        'widgetOptions' => [
        'pluginOptions' => [
            'autoclose' => true,
        ]],
    ]) ?>

    <?= $form->field($model, 'ended_at')->widget(DateControl::className(), [
        'type' => DateControl::FORMAT_DATETIME,
        'widgetOptions' => [
        'pluginOptions' => [
            'autoclose' => true,
        ]],
    ]) ?>

    <?= $form->field($model, 'personnel_leave')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'personnel_sick')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'personnel_off')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'personnel_overtime')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'field_information')->textarea(['rows' => 6]) ?>

    
    <div class="form-panel">
        <div class="row">
    	    <div class="col-sm-12">
    	        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . ($model->isNewRecord ? 'Create' : 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
	    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>

</div>
