<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\ChecksheetEquipment;

/* @var $this yii\web\View */
/* @var $model backend\models\Checksheet */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Checksheet', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="checksheet-view box-- box-info--">

    <div class="box-body--">
        <p>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> '. 'Update', ['update', 'id' => $model->id], [
            'class' => 'btn btn-warning',
        ]) ?>
        <?= Html::a('<i class="glyphicon glyphicon-trash"></i> ' . 'Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        </p>

        <div class="detail-view-container">
        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                // 'id',
                'field',
                'shift',
                'team',
                'started_at:datetime',
                'ended_at:datetime',
                'field_information:ntext',
                'personnel_leave',
                'personnel_sick',
                'personnel_off',
                'personnel_overtime',
                // 'created_at:datetime',
                // 'updated_at:datetime',
                // 'createdBy.username:text:Created By',
                // 'updatedBy.username:text:Updated By',
            ],
        ]) ?>
        </div>
    </div>
</div>

<?php 
$dataProvider = new \yii\data\ActiveDataProvider([
    'query' => ChecksheetEquipment::find()->where(['checksheet_id' => $model->id]),
    'pagination' => false,
    'sort' => [
        'defaultOrder' => [
            'id' => SORT_DESC,
        ]
    ],
]);

echo \kartik\grid\GridView::widget([
    'dataProvider' => $dataProvider,
                        // 'pjax' => true,
    'hover' => true,
    'striped' => false,
    'bordered' => false,
    'summary' => false,
    'toolbar' => [
                            // Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']),
        Html::a('<i class="fa fa-repeat"></i> ' . Yii::t('app', 'Reload'), ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default']),
        '{toggleData}',
                            // $exportMenu,
    ],
    'panel' => false,
    'pjaxSettings' => ['options' => ['id' => 'grid']],
    // 'filterModel' => $searchModel,
    // 'tableOptions' => ['class' => 'table table-condensed small'],
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['class' => 'text-right'],
            'contentOptions' => ['class' => 'text-right'],
        ],
        [
            'attribute' => 'equipment_id',
            'value' => 'equipment.name',
        ],
        'operator',
        [
            'attribute' => 'hourmeter_start',
            'format' => ['decimal', 2],
            'headerOptions' => ['class' => 'text-right'],
            'contentOptions' => ['class' => 'text-right'],
        ],
        [
            'attribute' => 'hourmeter_end',
            'format' => ['decimal', 2],
            'headerOptions' => ['class' => 'text-right'],
            'contentOptions' => ['class' => 'text-right'],
        ],
        [
            'attribute' => 'production',
            'format' => ['decimal', 2],
            'headerOptions' => ['class' => 'text-right'],
            'contentOptions' => ['class' => 'text-right'],
        ],
        [
            'attribute' => 'ritasi',
            'format' => 'integer',
            'headerOptions' => ['class' => 'text-right'],
            'contentOptions' => ['class' => 'text-right'],
        ],
        'location',
        [
            'attribute' => 'interruption_description',
        ],
        [
            'attribute' => 'interruption_started_at',
        ],
        [
            'attribute' => 'interruption_ended_at',
        ],
        [
            'attribute' => 'geometry_hole',
            'format' => ['decimal', 2],
            'headerOptions' => ['class' => 'text-right'],
            'contentOptions' => ['class' => 'text-right'],
        ],
        [
            'attribute' => 'geometry_space',
            'format' => ['decimal', 2],
            'headerOptions' => ['class' => 'text-right'],
            'contentOptions' => ['class' => 'text-right'],
        ],
        [
            'attribute' => 'geometry_burden',
            'format' => ['decimal', 2],
            'headerOptions' => ['class' => 'text-right'],
            'contentOptions' => ['class' => 'text-right'],
        ],
        [
            'attribute' => 'geometry_depth',
            'format' => ['decimal', 2],
            'headerOptions' => ['class' => 'text-right'],
            'contentOptions' => ['class' => 'text-right'],
        ],
    ],
]);
?>