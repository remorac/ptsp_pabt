<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Checksheet */

$this->title = 'Create Checksheet';
$this->params['breadcrumbs'][] = ['label' => 'Checksheet', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="checksheet-create box-- box-success--">
	<!-- <div class="box-header"></div> -->

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>
    
</div>
