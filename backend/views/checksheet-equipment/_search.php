<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ChecksheetEquipmentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="checksheet-equipment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'checksheet_id') ?>

    <?= $form->field($model, 'equipment_id') ?>

    <?= $form->field($model, 'operator') ?>

    <?= $form->field($model, 'hourmeter_start') ?>

    <?php // echo $form->field($model, 'hourmeter_end') ?>

    <?php // echo $form->field($model, 'interruption_description') ?>

    <?php // echo $form->field($model, 'interruption_started_at') ?>

    <?php // echo $form->field($model, 'interruption_ended_at') ?>

    <?php // echo $form->field($model, 'production') ?>

    <?php // echo $form->field($model, 'ritasi') ?>

    <?php // echo $form->field($model, 'location') ?>

    <?php // echo $form->field($model, 'geometry_hole') ?>

    <?php // echo $form->field($model, 'geometry_space') ?>

    <?php // echo $form->field($model, 'geometry_burden') ?>

    <?php // echo $form->field($model, 'geometry_depth') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
