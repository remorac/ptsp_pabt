<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\ChecksheetEquipment */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Checksheet Equipment', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="checksheet-equipment-view box-- box-info--">

    <div class="box-body--">
        <p>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> '. 'Update', ['update', 'id' => $model->id], [
            'class' => 'btn btn-warning',
        ]) ?>
        <?= Html::a('<i class="glyphicon glyphicon-trash"></i> ' . 'Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        </p>

        <div class="detail-view-container">
        <?= DetailView::widget([
            'options' => ['class' => 'table detail-view'],
            'model' => $model,
            'attributes' => [
                // 'id',
                'checksheet.name:text:Checksheet',
                'equipment.name:text:Equipment',
                'operator',
                'hourmeter_start:integer',
                'hourmeter_end:integer',
                'interruption_description:integer',
                'interruption_started_at:integer',
                'interruption_ended_at:integer',
                'production:integer',
                'ritasi:integer',
                'location',
                'geometry_hole:integer',
                'geometry_space:integer',
                'geometry_burden:integer',
                'geometry_depth:integer',
                // 'created_at:datetime',
                // 'updated_at:datetime',
                // 'createdBy.username:text:Created By',
                // 'updatedBy.username:text:Updated By',
            ],
        ]) ?>
        </div>
    </div>
</div>
