<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
use backend\models\Checksheet;
use backend\models\Equipment;

/* @var $this yii\web\View */
/* @var $model backend\models\ChecksheetEquipment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="checksheet-equipment-form">

<div class="row">
<div class="col-md-8 col-sm-12">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'checksheet_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Checksheet::find()->all(), 'id', 'name'),
        'options' => ['placeholder' => ''],
        'pluginOptions' => ['allowClear' => true],
    ]); ?>

    <?= $form->field($model, 'equipment_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Equipment::find()->all(), 'id', 'name'),
        'options' => ['placeholder' => ''],
        'pluginOptions' => ['allowClear' => true],
    ]); ?>

    <?= $form->field($model, 'operator')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hourmeter_start')->textInput() ?>

    <?= $form->field($model, 'hourmeter_end')->textInput() ?>

    <?= $form->field($model, 'interruption_description')->textInput() ?>

    <?= $form->field($model, 'interruption_started_at')->textInput() ?>

    <?= $form->field($model, 'interruption_ended_at')->textInput() ?>

    <?= $form->field($model, 'production')->textInput() ?>

    <?= $form->field($model, 'ritasi')->textInput() ?>

    <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'geometry_hole')->textInput() ?>

    <?= $form->field($model, 'geometry_space')->textInput() ?>

    <?= $form->field($model, 'geometry_burden')->textInput() ?>

    <?= $form->field($model, 'geometry_depth')->textInput() ?>

    
    <div class="form-panel">
        <div class="row">
    	    <div class="col-sm-12">
    	        <?= Html::submitButton('<i class="glyphicon glyphicon-ok"></i> ' . ($model->isNewRecord ? 'Create' : 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
	    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>

</div>
