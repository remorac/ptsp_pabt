<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ChecksheetEquipment */

$this->title = 'Update Checksheet Equipment: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Checksheet Equipment', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="checksheet-equipment-update box-- box-warning--">

    <!-- <div class="box-header"></div> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
