<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ChecksheetEquipment */

$this->title = 'Create Checksheet Equipment';
$this->params['breadcrumbs'][] = ['label' => 'Checksheet Equipment', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="checksheet-equipment-create box-- box-success--">
	<!-- <div class="box-header"></div> -->

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>
    
</div>
