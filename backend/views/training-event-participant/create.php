<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TrainingEventParticipant */

$this->title = 'Create Training Event Participant';
$this->params['breadcrumbs'][] = ['label' => 'Training Event Participant', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="training-event-participant-create box-- box-success--">
	<!-- <div class="box-header"></div> -->

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>
    
</div>
