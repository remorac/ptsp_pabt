<?php

namespace backend\controllers;

use Yii;
use backend\models\KpiFile;
use backend\models\KpiScore;
use backend\models\KpiFileSearch;
use backend\models\KpiScoreSearch;
use backend\models\Kpi;
use backend\models\KpiSearch;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\IntegrityException;
use yii\data\ActiveDataProvider;

/**
 * KpiController implements the CRUD actions for Kpi model.
 */
class KpiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-score' => ['POST'],
                    'delete-file' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Kpi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KpiSearch();
        $queryParams = Yii::$app->request->queryParams;
        if (!isset($queryParams['KpiSearch'])) $queryParams['KpiSearch'] = array();
        if (!isset($queryParams['KpiSearch']['year'])) $queryParams['KpiSearch']['year'] = date('Y');
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionReport()
    {
        $get = Yii::$app->request->get();
        $year = (isset($get['year']) && $get['year']) ? $get['year'] : date('Y');

        if (Kpi::findOne(['year' => $year]) === null) {
            Kpi::generate();
        }

        return $this->render('kpi', [
            'year' => $year,
        ]);
    }

    /**
     * Displays a single Kpi model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        
        $dataProviderFile = new ActiveDataProvider(['query' => KpiFile::find()->where(['kpi_id' => $id])->orderBy('id desc')]);
        $dataProviderScore = new ActiveDataProvider(['query' => KpiScore::find()->where(['kpi_id' => $id])->orderBy('id desc')]);

        $dataProviderFile->pagination = false;
        $dataProviderScore->pagination = false;

        $modelFile = new KpiFile();
        $modelScore = new KpiScore();

        $modelFile->kpi_id = $id;
        $modelScore->kpi_id = $id;

        $is_redirected = 0;

        if ($post = Yii::$app->request->post()) {
            if ($modelFile->load($post)) {
                $modelFile->pdfFile = UploadedFile::getInstance($modelFile, 'pdfFile');
                $modelFile->excelFile = UploadedFile::getInstance($modelFile, 'excelFile');
                
                if ($modelFile->save()) {
                    // print "<pre>"; print_r($modelFile); die();
                    if ($modelFile->pdfFile) {
                        $modelFile->pdf = $modelFile->tableName().'/'.$modelFile->id.'.'.$modelFile->pdfFile->extension;
                    }
                    if ($modelFile->excelFile) {
                        $modelFile->excel = $modelFile->tableName().'/'.$modelFile->id.'.'.$modelFile->excelFile->extension;
                    }
                    if ($modelFile->save()) {
                        if ($modelFile->pdfFile) $modelFile->pdfFile->saveAs(Yii::$app->params['uploadLocation'].$modelFile->pdf);
                        if ($modelFile->excelFile) $modelFile->excelFile->saveAs(Yii::$app->params['uploadLocation'].$modelFile->excel);
                    } else {
                        Yii::$app->session->addFlash('warning', 'Failed to save file.');
                    }
                    $is_redirected = 1;
                }
            }
            if ($modelScore->load($post)) {
                if ($modelScore->save()) { $is_redirected = 1; }
            }
        }

        if ($is_redirected) {
            return $this->redirect(['view', 'id' => $id]);
        }

        return $this->render('view', [
            'model' => $model,
            'modelFile' => $modelFile,
            'modelScore' => $modelScore,
            'dataProviderFile' => $dataProviderFile,
            'dataProviderScore' => $dataProviderScore,
        ]);
    }

    /**
     * Displays a single Kpi model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewReadonly($id)
    {
        $model = $this->findModel($id);
        
        $dataProviderFile = new ActiveDataProvider(['query' => KpiFile::find()->where(['kpi_id' => $id])->orderBy('id desc')]);
        $dataProviderScore = new ActiveDataProvider(['query' => KpiScore::find()->where(['kpi_id' => $id])->orderBy('id desc')]);

        $dataProviderFile->pagination = false;
        $dataProviderScore->pagination = false;

        return $this->render('view-readonly', [
            'model' => $model,
            'dataProviderFile' => $dataProviderFile,
            'dataProviderScore' => $dataProviderScore,
        ]);
    }

    /**
     * Creates a new Kpi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Kpi();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Kpi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Kpi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $model = $this->findModel($id);
            KpiScore::deleteAll(['kpi_id' => $id]);
            foreach ($model->kpiFiles as $kpiFile) {
                $excelFile = Yii::getAlias('@uploads/'.$kpiFile->excel);
                $pdfFile = Yii::getAlias('@uploads/'.$kpiFile->pdf);
                if (file_exists($excelFile) && is_file($excelFile)) unlink($excelFile);
                if (file_exists($pdfFile) && is_file($pdfFile)) unlink($pdfFile);
                unlink(Yii::getAlias('@uploads/'.$kpiFile->pdf));
            }
            KpiFile::deleteAll(['kpi_id' => $id]);
            $model->delete();
            return $this->redirect(['index']);
        } catch (IntegrityException $e) {
            throw new \yii\web\HttpException(500,"Integrity Constraint Violation. This data can not be deleted due to the relation.", 405);
        }
    }

    public function actionDeleteScore($id)
    {
        try {
            $model = KpiScore::findOne($id);
            $model->delete();
            return $this->redirect(['view', 'id' => $model->kpi_id]);
        } catch (IntegrityException $e) {
            throw new \yii\web\HttpException(500,"Integrity Constraint Violation. This data can not be deleted due to the relation.", 405);
        }
    }

    public function actionDeleteFile($id)
    {
        try {
            $model = KpiFile::findOne($id);
            $excelFile = Yii::getAlias('@uploads/'.$model->excel);
            $pdfFile = Yii::getAlias('@uploads/'.$model->pdf);
            if (file_exists($excelFile) && is_file($excelFile)) unlink($excelFile);
            if (file_exists($pdfFile) && is_file($pdfFile)) unlink($pdfFile);
            $model->delete();
            return $this->redirect(['view', 'id' => $model->kpi_id]);
        } catch (IntegrityException $e) {
            throw new \yii\web\HttpException(500,"Integrity Constraint Violation. This data can not be deleted due to the relation.", 405);
        }
    }

    /**
     * Finds the Kpi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Kpi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Kpi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionDownload($id, $filename) 
    {
        $model = KpiFile::findOne($id);
        $filepath = Yii::getAlias('@uploads/'.$filename);
        $extension = '.'.substr($filename, strpos($filename, ".") + 1);
        if (file_exists($filepath)) Yii::$app->response->sendFile($filepath, $model->kpi->name.' '.$model->date.$extension, ['inline'=>true]);
    }

    public function actionRemove($id, $filename) 
    {
        $filepath = Yii::getAlias('@uploads/'.$filename);
        if (file_exists($filepath)) {
            unlink($filepath);
        }
        return $this->redirect(['view', 'id' => $id]);
    }
}
