<?php

namespace backend\controllers;

use Yii;
use backend\models\Equipment;
use backend\models\Tire;
use backend\models\TireUsage;
use backend\models\TireUsageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\db\IntegrityException;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

/**
 * TireUsageController implements the CRUD actions for TireUsage model.
 */
class TireUsageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TireUsage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TireUsageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TireUsage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TireUsage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TireUsage();

        if ($model->load(Yii::$app->request->post())) {
            $model->is_first_usage = 1;
            if ($model->save()) {
                $path = Yii::getAlias('@uploads/tire-usage/' . $model->id . '/');
                $files = UploadedFile::getInstancesByName('files');

                if ($files) {
                    if (!is_dir($path)) {
                        mkdir($path, 0777, true);
                    }
                }
                foreach ($files as $file) {
                    $filename = $path . $file->name;
                    $i = 0;

                    while (file_exists($filename)) {
                        $i++;
                        $filename_sliced = explode('.', $filename);
                        $ext = end($filename_sliced);
                        $filename = $path . $file->name . ' (' . $i . ').' . $ext;
                    }
                    $file->saveAs($filename);
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TireUsage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                $path = Yii::getAlias('@uploads/tire-usage/' . $model->id . '/');
                $files = UploadedFile::getInstancesByName('files');

                if ($files) {
                    if (!is_dir($path)) {
                        mkdir($path, 0777, true);
                    }
                }
                foreach ($files as $file) {
                    $filename = $path . $file->name;
                    $i = 0;

                    while (file_exists($filename)) {
                        $i++;
                        $filename_sliced = explode('.', $filename);
                        $ext = end($filename_sliced);
                        $filename = $path . $file->name . ' (' . $i . ').' . $ext;
                    }
                    $file->saveAs($filename);
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TireUsage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        } catch (IntegrityException $e) {
            throw new \yii\web\HttpException(500,"Integrity Constraint Violation. This data can not be deleted due to the relation.", 405);
        }
    }

    /**
     * Finds the TireUsage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TireUsage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TireUsage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionImport()
    {
        if ($post = Yii::$app->request->post()) {
            $packageFile = UploadedFile::getInstanceByName('package-file');
            $reader = ReaderFactory::create(Type::XLSX);
            $reader->open($packageFile->tempName);

            $unsaved_rows = [];
            $saved_count = 0;

            foreach ($reader->getSheetIterator() as $sheet) {
                $rowCount = 0;
                foreach ($sheet->getRowIterator() as $row) {
                    $rowCount++;
                    if ($rowCount >= 2) {
                        $equipment = Equipment::findOne(['name' => trim((string)$row[0])]);
                        $tire = Tire::findOne(['name' => trim((string)$row[1])]);
                        
                        $model = new TireUsage();
                        $model->equipment_id    = $equipment ? $equipment->id : null;
                        $model->tire_id         = $tire ? $tire->id : null;
                        $model->serial          = trim((string)$row[2]);
                        $model->date            = isset($row[3]) ? (is_object($row[3]) ? $row[3]->format('Y-m-d') : null) : null;
                        $model->hourmeter       = intval($row[4]);
                        $model->cause           = trim((string)$row[5]);
                        $model->position        = trim((string)$row[6]);
                        $model->is_first_usage  = trim((string)$row[7]) == "Baru" ? 1 : 0;

                        if ($model->save()) {
                            $saved_count++;
                        } else {
                            $unsaved_rows[] = $rowCount;
                            Yii::$app->session->addFlash('error', $rowCount . ' :: ' . \yii\helpers\Json::encode($model->errors));
                        }
                    }
                }
            }
            $reader->close();
            $unsaved_rows_str = implode(', ', $unsaved_rows);
            if ($unsaved_rows) Yii::$app->session->setFlash(
                'warning',
                $saved_count . ' rows has been imported. 
                <br>You may want to re-check the following unsaved rows : ' . $unsaved_rows_str
            );
            return $this->redirect(['index']);
        } else {
            return $this->render('import');
        }
    }

    public function actionDownload($id, $filename)
    {
        $filepath = Yii::getAlias('@uploads/tire-usage/' . $id . '/' . $filename);
        if (file_exists($filepath)) Yii::$app->response->sendFile($filepath, $filename, ['inline' => true]);
    }

    public function actionRemove($id, $filename)
    {
        $filepath = Yii::getAlias('@uploads/tire-usage/' . $id . '/' . $filename);
        if (file_exists($filepath)) {
            unlink($filepath);
        }
        return $this->redirect(['update', 'id' => $id]);
    }
}
