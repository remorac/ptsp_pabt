<?php

namespace backend\controllers;

use Yii;
use backend\models\SolarUsage;
use backend\models\SolarUsageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\db\IntegrityException;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

/**
 * SolarUsageController implements the CRUD actions for SolarUsage model.
 */
class SolarUsageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SolarUsage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SolarUsageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SolarUsage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SolarUsage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SolarUsage();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SolarUsage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SolarUsage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        } catch (IntegrityException $e) {
            throw new \yii\web\HttpException(500,"Integrity Constraint Violation. This data can not be deleted due to the relation.", 405);
        }
    }

    /**
     * Finds the SolarUsage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SolarUsage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SolarUsage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionImport()
    {
        if ($post = Yii::$app->request->post()) {
            $packageFile = UploadedFile::getInstanceByName('package-file');
            $reader = ReaderFactory::create(Type::XLSX);
            $reader->open($packageFile->tempName);

            $unsaved_rows = [];
            $saved_count = 0;

            foreach ($reader->getSheetIterator() as $sheet) {
                $rowCount = 0;
                foreach ($sheet->getRowIterator() as $row) {
                    $rowCount++;
                    if ($rowCount >= 2) {
                        $equipment = Equipment::findOne(['name' => trim((string)$row[0])]);
                        
                        $model = new SolarUsage();
                        $model->equipment_id        = $equipment ? $equipment->id : null;
                        $model->solar_truck_name    = trim((string)$row[1]);
                        $model->date                = isset($row[2]) ? (is_object($row[2]) ? $row[2]->format('Y-m-d') : null) : null;
                        $model->shift               = intval($row[3]);
                        $model->volume              = floatval($row[4]);
                        $model->hourmeter           = floatval($row[5]);
                        $model->flowmeter_before    = floatval($row[6]);
                        $model->flowmeter_after     = floatval($row[7]);
                        $model->operator_equipment  = trim((string)$row[8]);
                        $model->operator_solar_truck = trim((string)$row[9]);

                        if ($model->save()) {
                            $saved_count++;
                        } else {
                            $unsaved_rows[] = $rowCount;
                            Yii::$app->session->addFlash('error', $rowCount . ' :: ' . \yii\helpers\Json::encode($model->errors));
                        }
                    }
                }
            }
            $reader->close();
            $unsaved_rows_str = implode(', ', $unsaved_rows);
            if ($unsaved_rows) Yii::$app->session->setFlash(
                'warning',
                $saved_count . ' rows has been imported. 
                <br>You may want to re-check the following unsaved rows : ' . $unsaved_rows_str
            );
            return $this->redirect(['index']);
        } else {
            return $this->render('import');
        }
    }
}
