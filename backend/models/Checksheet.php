<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "checksheet".
 *
 * @property integer $id
 * @property string $field
 * @property string $shift
 * @property string $team
 * @property integer $started_at
 * @property integer $ended_at
 * @property string $field_information
 * @property string $personnel_leave
 * @property string $personnel_sick
 * @property string $personnel_off
 * @property string $personnel_overtime
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property ChecksheetEquipment[] $checksheetEquipments
 */
class Checksheet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'checksheet';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['field', 'shift', 'team', 'started_at', 'ended_at'], 'required'],
            [['started_at', 'ended_at', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['field_information'], 'string'],
            [['field', 'shift', 'team', 'personnel_leave', 'personnel_sick', 'personnel_off', 'personnel_overtime'], 'string', 'max' => 191],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'field' => 'Bidang',
            'shift' => 'Shift',
            'team' => 'Group',
            'started_at' => 'Started At',
            'ended_at' => 'Ended At',
            'field_information' => 'Information',
            'personnel_leave' => 'Cuti',
            'personnel_sick' => 'Sakit',
            'personnel_off' => 'Off',
            'personnel_overtime' => 'Lembur',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChecksheetEquipments()
    {
        return $this->hasMany(ChecksheetEquipment::className(), ['checksheet_id' => 'id']);
    }
}
