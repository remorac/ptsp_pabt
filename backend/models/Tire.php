<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tire".
 *
 * @property integer $id
 * @property string $name
 * @property string $brand
 * @property string $type
 * @property string $size
 * @property integer $quantity
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property TireIn[] $tireIns
 * @property TireUsage[] $tireUsages
 */
class Tire extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tire';
    }

    public static function addStock($id, $quantity)
    {
        $model = self::findOne($id);
        $model->quantity = $model->quantity + $quantity;
        return $model->save() ? true : false;
    }

    public static function subtractStock($id, $quantity)
    {
        $model = self::findOne($id);
        $model->quantity = $model->quantity - $quantity;
        return $model->save() ? true : false;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['quantity', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name', 'brand', 'type', 'size'], 'string', 'max' => 191],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'brand' => 'Brand',
            'type' => 'Type',
            'size' => 'Size',
            'quantity' => 'Quantity',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTireIns()
    {
        return $this->hasMany(TireIn::className(), ['tire_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTireUsages()
    {
        return $this->hasMany(TireUsage::className(), ['tire_id' => 'id']);
    }
}
