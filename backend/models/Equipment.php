<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "equipment".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property string $brand
 * @property string $purpose
 * @property string $owner
 * @property double $fuel_consumption_low
 * @property double $fuel_consumption_medium
 * @property double $fuel_consumption_high
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property ChecksheetEquipment[] $checksheetEquipments
 * @property SolarUsage[] $solarUsages
 * @property TireUsage[] $tireUsages
 */
class Equipment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'equipment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type', 'brand'], 'required'],
            [['fuel_consumption_low', 'fuel_consumption_medium', 'fuel_consumption_high'], 'number'],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 191],
            [['type', 'brand', 'purpose', 'owner'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'brand' => 'Brand',
            'purpose' => 'Purpose',
            'owner' => 'Owner',
            'fuel_consumption_low' => 'Fuel Consumption Low',
            'fuel_consumption_medium' => 'Fuel Consumption Medium',
            'fuel_consumption_high' => 'Fuel Consumption High',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChecksheetEquipments()
    {
        return $this->hasMany(ChecksheetEquipment::className(), ['equipment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolarUsages()
    {
        return $this->hasMany(SolarUsage::className(), ['equipment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTireUsages()
    {
        return $this->hasMany(TireUsage::className(), ['equipment_id' => 'id']);
    }

    public function getNameWithLastHourmeter() {
        $last_hourmeter = Hourmeter::find()->where(['equipment' => $this->name])->max('hourmeter');
        if ($last_hourmeter) return $this->name . ' (lastHM: ' . Yii::$app->formatter->asDecimal($last_hourmeter, 0) . ')';
        return $this->name;
    }
}
