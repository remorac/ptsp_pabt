<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Checksheet;

/**
 * ChecksheetSearch represents the model behind the search form about `backend\models\Checksheet`.
 */
class ChecksheetSearch extends Checksheet
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'started_at', 'ended_at', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['field', 'shift', 'team', 'field_information', 'personnel_leave', 'personnel_sick', 'personnel_off', 'personnel_overtime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Checksheet::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id'=>SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'started_at' => $this->started_at,
            'ended_at' => $this->ended_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'field', $this->field])
            ->andFilterWhere(['like', 'shift', $this->shift])
            ->andFilterWhere(['like', 'team', $this->team])
            ->andFilterWhere(['like', 'field_information', $this->field_information])
            ->andFilterWhere(['like', 'personnel_leave', $this->personnel_leave])
            ->andFilterWhere(['like', 'personnel_sick', $this->personnel_sick])
            ->andFilterWhere(['like', 'personnel_off', $this->personnel_off])
            ->andFilterWhere(['like', 'personnel_overtime', $this->personnel_overtime]);

        return $dataProvider;
    }
}
