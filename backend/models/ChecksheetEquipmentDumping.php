<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "checksheet_equipment_dumping".
 *
 * @property integer $id
 * @property integer $checksheet_equipment_id
 * @property string $location
 * @property integer $amount
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property ChecksheetEquipment $checksheetEquipment
 * @property User $createdBy
 * @property User $updatedBy
 */
class ChecksheetEquipmentDumping extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'checksheet_equipment_dumping';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['checksheet_equipment_id', 'location', 'amount'], 'required'],
            [['checksheet_equipment_id', 'amount', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['location'], 'string', 'max' => 191],
            [['checksheet_equipment_id'], 'exist', 'skipOnError' => true, 'targetClass' => ChecksheetEquipment::className(), 'targetAttribute' => ['checksheet_equipment_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'checksheet_equipment_id' => 'Checksheet Equipment',
            'location' => 'Location',
            'amount' => 'Amount',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChecksheetEquipment()
    {
        return $this->hasOne(ChecksheetEquipment::className(), ['id' => 'checksheet_equipment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
