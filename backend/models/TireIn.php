<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tire_in".
 *
 * @property integer $id
 * @property integer $tire_id
 * @property integer $quantity
 * @property string $date
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Tire $tire
 * @property User $createdBy
 * @property User $updatedBy
 */
class TireIn extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tire_in';
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        Tire::addStock($this->tire_id, ($this->quantity - $changedAttributes['quantity']));
    }

    public function afterDelete()
    {
        parent::afterDelete();
        Tire::subtractStock($this->tire_id, $this->quantity);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tire_id', 'quantity', 'date'], 'required'],
            [['tire_id', 'quantity', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['date'], 'safe'],
            [['tire_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tire::className(), 'targetAttribute' => ['tire_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tire_id' => 'Tire',
            'quantity' => 'Quantity',
            'date' => 'Date',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTire()
    {
        return $this->hasOne(Tire::className(), ['id' => 'tire_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
