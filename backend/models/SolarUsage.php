<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "solar_usage".
 *
 * @property integer $id
 * @property integer $equipment_id
 * @property string $solar_truck_name
 * @property string $date
 * @property integer $shift
 * @property double $volume
 * @property double $hourmeter
 * @property integer $flowmeter_before
 * @property integer $flowmeter_after
 * @property string $operator_equipment
 * @property string $operator_solar_truck
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property Equipment $equipment
 */
class SolarUsage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'solar_usage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['equipment_id', 'solar_truck_name', 'date', 'shift', 'volume'], 'required'],
            [['equipment_id', 'shift', 'flowmeter_before', 'flowmeter_after', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['date'], 'safe'],
            [['volume', 'hourmeter'], 'number'],
            [['solar_truck_name', 'operator_equipment', 'operator_solar_truck'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['equipment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Equipment::className(), 'targetAttribute' => ['equipment_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'equipment_id' => 'Equipment',
            'solar_truck_name' => 'Solar Truck Name',
            'date' => 'Date',
            'shift' => 'Shift',
            'volume' => 'Volume (ltr)',
            'hourmeter' => 'Hourmeter',
            'flowmeter_before' => 'Flowmeter Before',
            'flowmeter_after' => 'Flowmeter After',
            'operator_equipment' => 'Operator Equipment',
            'operator_solar_truck' => 'Operator Solar Truck',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEquipment()
    {
        return $this->hasOne(Equipment::className(), ['id' => 'equipment_id']);
    }
}
