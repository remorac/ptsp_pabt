<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "kpi_score".
 *
 * @property integer $id
 * @property integer $kpi_id
 * @property integer $month
 * @property string $value
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Kpi $kpi
 * @property User $createdBy
 * @property User $updatedBy
 */
class KpiScore extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kpi_score';
    }

    public static function month($id = '') {
        $months = [];
        for ($i = 1; $i <= 12 ; $i++) { 
            $months[$i] = date('F', strtotime(date('Y-'.$i.'-d')));
        }
        if ($id >=1 && $id <= 12) return $months[$id];
        if ($id == 'all') return $months;
        return;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kpi_id', 'month'], 'required'],
            [['kpi_id', 'month', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['value'], 'string', 'max' => 191],
            [['kpi_id', 'month'], 'unique', 'targetAttribute' => ['kpi_id', 'month'], 'message' => 'The combination of Kpi and Month has already been taken.'],
            [['kpi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kpi::className(), 'targetAttribute' => ['kpi_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kpi_id' => 'Kpi',
            'month' => 'Month',
            'value' => 'Value',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKpi()
    {
        return $this->hasOne(Kpi::className(), ['id' => 'kpi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
