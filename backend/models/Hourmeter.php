<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "_hourmeter".
 *
 * @property string $equipment
 * @property double $hourmeter
 * @property string $date
 */
class Hourmeter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '_hourmeter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hourmeter'], 'number'],
            [['equipment'], 'string', 'max' => 191],
            [['date'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'equipment' => 'Equipment',
            'hourmeter' => 'Hourmeter',
            'date' => 'Date',
        ];
    }
}
