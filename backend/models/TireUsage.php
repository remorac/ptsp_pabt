<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tire_usage".
 *
 * @property integer $id
 * @property integer $equipment_id
 * @property integer $tire_id
 * @property string $serial
 * @property string $date
 * @property integer $hourmeter
 * @property string $cause
 * @property string $position
 * @property integer $is_first_usage
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Equipment $equipment
 * @property Tire $tire
 * @property User $createdBy
 * @property User $updatedBy
 */
class TireUsage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tire_usage';
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        Tire::subtractStock($this->tire_id, 1);
    }

    public function afterDelete()
    {
        parent::afterDelete();
        Tire::addStock($this->tire_id, 1);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['equipment_id', 'tire_id', 'serial', 'date', 'hourmeter'], 'unique', 'targetAttribute' => ['equipment_id', 'tire_id', 'serial', 'date', 'hourmeter']],
            [['equipment_id', 'tire_id', 'serial', 'date'], 'required'],
            [['equipment_id', 'tire_id', 'hourmeter', 'is_first_usage', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['date'], 'safe'],
            [['remark'], 'string'],
            [['serial', 'cause', 'position'], 'string', 'max' => 191],
            [['equipment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Equipment::className(), 'targetAttribute' => ['equipment_id' => 'id']],
            [['tire_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tire::className(), 'targetAttribute' => ['tire_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'equipment_id' => 'Equipment',
            'tire_id' => 'Tire',
            'serial' => 'Serial',
            'date' => 'Date',
            'hourmeter' => 'Hourmeter',
            'cause' => 'Cause',
            'position' => 'Position',
            'is_first_usage' => 'Is First Usage',
            'remark' => 'Remark',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEquipment()
    {
        return $this->hasOne(Equipment::className(), ['id' => 'equipment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTire()
    {
        return $this->hasOne(Tire::className(), ['id' => 'tire_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
