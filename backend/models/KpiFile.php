<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "kpi_file".
 *
 * @property integer $id
 * @property integer $kpi_id
 * @property string $excel
 * @property string $pdf
 * @property string $date
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Kpi $kpi
 * @property User $createdBy
 * @property User $updatedBy
 */
class KpiFile extends \yii\db\ActiveRecord
{
    public $pdfFile;
    public $excelFile;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kpi_file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kpi_id', 'date'], 'required'],
            [['kpi_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['excel', 'pdf'], 'string'],
            [['date'], 'safe'],
            [['kpi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kpi::className(), 'targetAttribute' => ['kpi_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['pdfFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf, PDF', 'mimeTypes' => 'application/pdf', /* 'maxSize' => 8388608, */ 'maxFiles' => 1],
            [['excelFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xlsx, XLSX', 'mimeTypes' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', /* 'maxSize' => 8388608, */ 'maxFiles' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kpi_id' => 'Kpi',
            'excel' => 'Excel',
            'pdf' => 'Pdf',
            'date' => 'Date',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKpi()
    {
        return $this->hasOne(Kpi::className(), ['id' => 'kpi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
