<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "training_event".
 *
 * @property integer $id
 * @property string $name
 * @property integer $training_field_id
 * @property string $start_date
 * @property string $end_date
 * @property string $location
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property TrainingField $trainingField
 * @property TrainingEventParticipant[] $trainingEventParticipants
 */
class TrainingEvent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'training_event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'training_field_id', 'start_date', 'end_date', 'location'], 'required'],
            [['training_field_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['start_date', 'end_date'], 'safe'],
            [['name'], 'string', 'max' => 191],
            [['location'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['training_field_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingField::className(), 'targetAttribute' => ['training_field_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'training_field_id' => 'Training Field',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'location' => 'Location',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainingField()
    {
        return $this->hasOne(TrainingField::className(), ['id' => 'training_field_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainingEventParticipants()
    {
        return $this->hasMany(TrainingEventParticipant::className(), ['training_event_id' => 'id']);
    }
}
