<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\SolarUsage;

/**
 * SolarUsageSearch represents the model behind the search form about `backend\models\SolarUsage`.
 */
class SolarUsageSearch extends SolarUsage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'equipment_id', 'shift', 'flowmeter_before', 'flowmeter_after', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['solar_truck_name', 'date', 'operator_equipment', 'operator_solar_truck'], 'safe'],
            [['volume', 'hourmeter'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SolarUsage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id'=>SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'equipment_id' => $this->equipment_id,
            'date' => $this->date,
            'shift' => $this->shift,
            'volume' => $this->volume,
            'hourmeter' => $this->hourmeter,
            'flowmeter_before' => $this->flowmeter_before,
            'flowmeter_after' => $this->flowmeter_after,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'solar_truck_name', $this->solar_truck_name])
            ->andFilterWhere(['like', 'operator_equipment', $this->operator_equipment])
            ->andFilterWhere(['like', 'operator_solar_truck', $this->operator_solar_truck]);

        return $dataProvider;
    }
}
