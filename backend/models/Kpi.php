<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "kpi".
 *
 * @property integer $id
 * @property string $name
 * @property integer $year
 * @property string $target
 * @property string $base_unit
 * @property string $polarization
 * @property string $period_type
 * @property integer $is_excluded
 * @property integer $is_active
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property KpiFile[] $kpiFiles
 * @property KpiScore[] $kpiScores
 */
class Kpi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kpi';
    }

    public static function isExcluded($id = '') {
        switch ($id) {
            case '0'    : return 'No'; break;
            case '1'    : return 'Yes'; break;
            case 'all'  : return ['0' => 'No', '1' => 'Yes']; break;
            default     : return; break;
        }
    }

    public static function isActive($id = '') {
        switch ($id) {
            case '0'    : return 'No'; break;
            case '1'    : return 'Yes'; break;
            case 'all'  : return ['1' => 'Yes', '0' => 'No']; break;
            default     : return; break;
        }
    }

    public static function polarization($id = '') {
        switch ($id) {
            case 'Min'    : return 'Min'; break;
            case 'Max'    : return 'Max'; break;
            case 'all'  : return ['Min' => 'Min', 'Max' => 'Max']; break;
            default     : return; break;
        }
    }

    public static function periodType($id = '') {
        switch ($id) {
            case '1'    : return 'Monthly'; break;
            case '3'    : return 'Quarterly'; break;
            case '12'    : return 'Annually'; break;
            case 'all'  : return ['1' => 'Monthly', '3' => 'Quarterly', '12' => 'Annually']; break;            
            default     : return; break;
        }
    }

    public static function generate() {
        $current_year = date('Y');
        $max_existed_year = Kpi::find()->max('year');
        Kpi::deleteAll(['year' => $current_year]);
        if ($max_existed_year) {
            $old_models = Kpi::findAll(['year' => $max_existed_year]);
            foreach ($old_models as $old_model) {
                $new_model = new Kpi();
                $new_model->name            = $old_model->name;
                $new_model->year            = $current_year;
                $new_model->target          = $old_model->target;
                $new_model->base_unit       = $old_model->base_unit;
                $new_model->polarization    = $old_model->polarization;
                $new_model->period_type     = $old_model->period_type;
                $new_model->is_excluded     = $old_model->is_excluded;
                $new_model->is_active       = $old_model->is_active;
                $new_model->save();
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'year'], 'required'],
            [['year', 'is_excluded', 'is_active', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name', 'target', 'base_unit', 'polarization', 'period_type'], 'string', 'max' => 191],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'year' => 'Year',
            'target' => 'Target',
            'base_unit' => 'Base Unit',
            'polarization' => 'Polarization',
            'period_type' => 'Period Type',
            'is_excluded' => 'Is Excluded',
            'is_active' => 'Is Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKpiFiles()
    {
        return $this->hasMany(KpiFile::className(), ['kpi_id' => 'id'])->orderBy(['date' => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKpiScores()
    {
        return $this->hasMany(KpiScore::className(), ['kpi_id' => 'id'])->orderBy(['month' => SORT_ASC]);
    }
}
