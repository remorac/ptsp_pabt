<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ChecksheetEquipment;

/**
 * ChecksheetEquipmentSearch represents the model behind the search form about `backend\models\ChecksheetEquipment`.
 */
class ChecksheetEquipmentSearch extends ChecksheetEquipment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'checksheet_id', 'equipment_id', 'hourmeter_start', 'hourmeter_end', 'interruption_description', 'interruption_started_at', 'interruption_ended_at', 'production', 'ritasi', 'geometry_hole', 'geometry_space', 'geometry_burden', 'geometry_depth', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['operator', 'location'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ChecksheetEquipment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id'=>SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'checksheet_id' => $this->checksheet_id,
            'equipment_id' => $this->equipment_id,
            'hourmeter_start' => $this->hourmeter_start,
            'hourmeter_end' => $this->hourmeter_end,
            'interruption_description' => $this->interruption_description,
            'interruption_started_at' => $this->interruption_started_at,
            'interruption_ended_at' => $this->interruption_ended_at,
            'production' => $this->production,
            'ritasi' => $this->ritasi,
            'geometry_hole' => $this->geometry_hole,
            'geometry_space' => $this->geometry_space,
            'geometry_burden' => $this->geometry_burden,
            'geometry_depth' => $this->geometry_depth,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'operator', $this->operator])
            ->andFilterWhere(['like', 'location', $this->location]);

        return $dataProvider;
    }
}
