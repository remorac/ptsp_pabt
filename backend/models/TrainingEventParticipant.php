<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "training_event_participant".
 *
 * @property integer $id
 * @property integer $training_event_id
 * @property integer $training_participant_id
 * @property integer $is_approved
 * @property string $remark
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property TrainingEvent $trainingEvent
 * @property TrainingParticipant $trainingParticipant
 * @property User $createdBy
 * @property User $updatedBy
 */
class TrainingEventParticipant extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'training_event_participant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['training_event_id', 'training_participant_id'], 'required'],
            [['training_event_id', 'training_participant_id', 'is_approved', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['remark'], 'string'],
            [['training_event_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingEvent::className(), 'targetAttribute' => ['training_event_id' => 'id']],
            [['training_participant_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainingParticipant::className(), 'targetAttribute' => ['training_participant_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'training_event_id' => 'Training Event',
            'training_participant_id' => 'Training Participant',
            'is_approved' => 'Is Approved',
            'remark' => 'Remark',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainingEvent()
    {
        return $this->hasOne(TrainingEvent::className(), ['id' => 'training_event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainingParticipant()
    {
        return $this->hasOne(TrainingParticipant::className(), ['id' => 'training_participant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
