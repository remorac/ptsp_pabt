<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "checksheet_equipment".
 *
 * @property integer $id
 * @property integer $checksheet_id
 * @property integer $equipment_id
 * @property string $operator
 * @property double $hourmeter_start
 * @property double $hourmeter_end
 * @property string $interruption_description
 * @property string $interruption_started_at
 * @property string $interruption_ended_at
 * @property double $production
 * @property integer $ritasi
 * @property string $location
 * @property double $geometry_hole
 * @property double $geometry_space
 * @property double $geometry_burden
 * @property double $geometry_depth
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Checksheet $checksheet
 * @property Equipment $equipment
 * @property User $createdBy
 * @property User $updatedBy
 * @property ChecksheetEquipmentDumping[] $checksheetEquipmentDumpings
 */
class ChecksheetEquipment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \yii\behaviors\BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'checksheet_equipment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['checksheet_id', 'equipment_id', 'operator'], 'required'],
            [['checksheet_id', 'equipment_id', 'ritasi', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['hourmeter_start', 'hourmeter_end', 'production', 'geometry_hole', 'geometry_space', 'geometry_burden', 'geometry_depth'], 'number'],
            [['interruption_description'], 'string'],
            [['operator', 'interruption_started_at', 'interruption_ended_at', 'location'], 'string', 'max' => 191],
            [['checksheet_id'], 'exist', 'skipOnError' => true, 'targetClass' => Checksheet::className(), 'targetAttribute' => ['checksheet_id' => 'id']],
            [['equipment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Equipment::className(), 'targetAttribute' => ['equipment_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'checksheet_id' => 'Checksheet',
            'equipment_id' => 'Equipment',
            'operator' => 'Operator',
            'hourmeter_start' => 'HM Awal',
            'hourmeter_end' => 'HM Akhir',
            'interruption_description' => 'Gangguan',
            'interruption_started_at' => 'Mulai Gangguan',
            'interruption_ended_at' => 'Selesai Gangguan',
            'production' => 'Produksi (Ton/Jam)',
            'ritasi' => 'Ritasi',
            'location' => 'Lokasi',
            'geometry_hole' => 'Geometri - Lubang',
            'geometry_space' => 'Geometri - Spasi',
            'geometry_burden' => 'Geometri - Burden',
            'geometry_depth' => 'Geometri - Dalam',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChecksheet()
    {
        return $this->hasOne(Checksheet::className(), ['id' => 'checksheet_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEquipment()
    {
        return $this->hasOne(Equipment::className(), ['id' => 'equipment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChecksheetEquipmentDumpings()
    {
        return $this->hasMany(ChecksheetEquipmentDumping::className(), ['checksheet_equipment_id' => 'id']);
    }
}
